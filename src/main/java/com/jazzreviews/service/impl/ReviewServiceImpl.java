
package com.jazzreviews.service.impl;

import com.jazzreviews.entity.Category;
import com.jazzreviews.entity.Review;
import com.jazzreviews.entity.User;
import com.jazzreviews.repository.ReviewRepository;
import com.jazzreviews.service.ReviewService;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ReviewServiceImpl implements ReviewService {
    
    @Autowired
    ReviewRepository reviewRepository;

    
    @Override
    public Review saveReview(Review r) {
        return reviewRepository.save(r);
    }
    
    @Override
    public List<Review> findAll() {
        
        return reviewRepository.findAll();
        
    }

    @Override
    public Review findById(Long id) {
        return reviewRepository.findById(id);
    }


    @Override
    public List<Review> findByAuthor(User user) {
        return reviewRepository.findByAuthor(user);
    }

    @Override
    public void deleteReview(Long id) {
        reviewRepository.delete(id);
    }

    @Override
    public List<Review> seachReviews(String searchQuery) {
         List<Review> toReturn = reviewRepository.findByTitleContaining(searchQuery);
         toReturn.addAll(reviewRepository.findByContentContaining(searchQuery));
         return toReturn;
    }

    @Override
    public List<Review> findByCategory(Category chosenCategory) {
        return reviewRepository.findByCategory(chosenCategory);
    }

    @Override
    public List<Review> findSixReviews() {
        return reviewRepository.findSixReviews();
    }

    @Override
    public List<Review> findTop9ByOrderByLikesDesc() {
        return reviewRepository.findTop9ByOrderByLikesDesc();
    }




}
