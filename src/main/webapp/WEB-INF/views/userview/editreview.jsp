

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>modifier review</title>
        <c:import url="../includes/bootstrap.jsp" />
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
    </head>
    <body>
        <c:import url="../includes/navbar.jsp" />
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1>modifier une review</h1>
                <div class="text-center"><img width="500" src="<c:url value="/download/reviewpic/${foundReview.id}"/>" /></div>

                <c:url var="addUrl" value="/user/upload/reviewpic"/>
                <form:form method="post" action="${addUrl}" enctype="multipart/form-data" modelAttribute="fileModel">
                    <div class="form-group" >
                        <input name="reviewid" type="hidden" class="form-control" value="${foundReview.id}" />
                        <form:input path="file" type="file" class="form-control" accept="image/*" />

                        <input type="submit" value="upload" class="btn btn-primary" />
                    </div>
                </form:form>

                <form role="form" method="post" action="<c:url value="/user/editreview" />" >
                    <input type="hidden" name="id" value="${foundReview.id}" />
                    <div class="form-group">
                        <label for="title">titre</label>
                        <input class="form-control" id="title" type="text" name="title" required value="${foundReview.title}" />
                    </div>
                    <div class="form-group">
                        <label for="comment">Votre review:</label>
                        <textarea class="form-control" rows="8" id="comment" name="review">${foundReview.content}</textarea>
                    </div>
                    <div class="row">
                        <div><button type="submit" class="btn btn-default">Submit</button></div>
                        <div class="text-right"><a href="<c:url value="/user/deletereview/${foundReview.id}" />"><button onclick="return areYouSure()" type="button" class="btn btn-danger">Supprimer</button></a></div>
                    </div>
                </form>
                <br/>

            </div>
        </div>
        <script>
            function areYouSure() {
                var ok = confirm("veuillez confirmer la suppresion");
                return ok;
            }

        </script>
    </body>
</html>
