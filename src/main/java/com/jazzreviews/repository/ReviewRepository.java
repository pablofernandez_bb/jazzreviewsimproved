/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jazzreviews.repository;

import com.jazzreviews.entity.Category;
import com.jazzreviews.entity.Review;
import com.jazzreviews.entity.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Student
 */
public interface ReviewRepository extends JpaRepository<Review,Long> {

    Review findById(Long id);
    
    
    
    List<Review> findByAuthor(User user);

    List<Review> findByTitleContaining(String query);
    
    List<Review> findByContentContaining(String query);

    List<Review> findByCategory(Category chosenCategory);

    @Query(value = "SELECT * FROM review ORDER BY creationDate DESC LIMIT 6", nativeQuery = true)
    List<Review> findSixReviews();

    List<Review> findTop9ByOrderByLikesDesc();



}
