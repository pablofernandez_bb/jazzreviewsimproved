
package com.jazzreviews.service;

import com.jazzreviews.entity.Category;
import com.jazzreviews.entity.Review;
import com.jazzreviews.entity.User;
import java.util.List;
import java.util.Set;


public interface ReviewService {
    
    Review saveReview(Review r);
    
    List<Review> findAll();

    Review findById(Long id);
        
    List<Review> findByAuthor(User user);

    void deleteReview(Long id);

    List<Review> seachReviews(String searchQuery);

    List<Review> findByCategory(Category chosenCategory);

    List<Review> findSixReviews();

    List<Review> findTop9ByOrderByLikesDesc();


}
