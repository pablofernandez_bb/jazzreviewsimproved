
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>mes reviews</title>
        <c:import url="../includes/bootstrap.jsp" />
    </head>
    <body>
        <c:import url="../includes/navbar.jsp" />
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1 class="text-center">mes reviews</h1>
                <p><em>cliquer pour éditer/supprimer</em></p>

                <div class="list-group">
                    <c:forEach var="myReviews" items="${myReviews}">
                        <a href="<c:url value="/user/editreview/${myReviews.id}"/>" class="list-group-item">${myReviews.title} <em>(écrit le ${myReviews.creationDate})</em></a>
                    </c:forEach>
                </div>
            </div>
        </div>
    </body>
</html>
