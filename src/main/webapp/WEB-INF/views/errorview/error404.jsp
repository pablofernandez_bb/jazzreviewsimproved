
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>404</title>
    <c:import url="../includes/bootstrap.jsp" />
</head>
<body>
<c:import url="../includes/navbar.jsp" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">

        <div class="text-center"><img src="<c:url value="/resources/img/404.jpg"/>" /></div>
        <p class="text-center">la page n'a pas été trouvée :( </p>
    </div>


</div>

</body>
</html>