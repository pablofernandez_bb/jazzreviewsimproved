<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${review.title}</title>
    <c:import url="../includes/bootstrap.jsp"/>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
<c:import url="../includes/navbar.jsp"/>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="well">
            <sec:authorize access="isAuthenticated()">
                <div class="pull-right like"><a href="#"><span class="likeText">liker cette review </span><span
                        class="glyphicon glyphicon-heart"> </span> <span class="counter">${review.likes}</span></a>
                </div>
            </sec:authorize>
            <sec:authorize access="isAnonymous()">
                <div class="pull-right"><span class="glyphicon glyphicon-heart"></span> ${review.likes}</div>
            </sec:authorize>
            <h1>${review.title}</h1>

            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <div class="text-right">
                    <a href="<c:url value="/admin/deletereview/${review.id}" />">
                        <button onclick="return areYouSure()" type="button" class="btn btn-danger">supprimer la review
                        </button>
                    </a>
                </div>
            </sec:authorize>
            <p>par<strong><a href="<c:url value="/profilepage/${review.author}" />"> ${review.author}</a></strong></p>
            <p>écrit le : <fmt:formatDate value="${review.creationDate}" pattern="dd-MM-yyyy"/></p>
            <p>catégorie : <strong><a
                    href="<c:url value="/showcategory/${review.category.name}"/>"> ${review.category.name}</a></strong>
            </p>

            <div class="text-center"><img class="img-thumbnail reviewPic"
                                          src="<c:url value="/download/reviewpic/${review.id}"/>"/></div>

            <p>${review.content}</p>

            <p class="pull-right"><strong><a href="<c:url value="/reviewsbyauthor/${review.author}"/>">voir d'autres reviews de ${review.author}</a></strong></p>
            <br/>
        </div>

        <sec:authorize access="isAnonymous()">
            Vous devez vous <a href="<c:url value="/signup" />">enregistrer</a> ou vous <a
                href="<c:url value="/login" />">identifier</a> pour poster un commentaire
        </sec:authorize>

        <h3>commentaires : (${review.comments.size()})</h3>
        <c:if test="${review.comments.size() == 0}">
            <br/>
            <p class="text-center"><strong>pas de commentaires pour cette review</strong></p>
        </c:if>
        <c:forEach var="comment" items="${review.comments}">
            <div class="well">
                <p><strong><a href="<c:url value="/profilepage/${comment.author}" />"> ${comment.author}</a></strong>,
                    le <fmt:formatDate value="${comment.creationDate}" pattern="dd-MM-yyyy"/>, à <fmt:formatDate value="${comment.creationDate}" pattern="HH:mm"/></p>
                <p>${comment.content}</p>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <div class="text-right">
                        <a href="<c:url value="/admin/deletecomm/${review.id}/${comment.id}" />">
                            <button onclick="return areYouSure()" type="button" class="btn btn-danger">supprimer
                            </button>
                        </a>
                    </div>
                </sec:authorize>
            </div>
        </c:forEach>

        <sec:authorize access="isAuthenticated()">

            <form role="form" method="post" action="<c:url value="/user/writecomment" />">
                <div class="form-group">
                    <input type="hidden" name="reviewid" value="${review.id}"/>
                    <label for="comment">Votre commentaire:</label>
                    <textarea class="form-control" rows="8" id="comment" name="comment"></textarea>
                </div>
                <button type="submit" class="btn btn-default">envoyer</button>
            </form>

        </sec:authorize>


    </div>
</div>
<script>
    function areYouSure() {
        var ok = confirm("veuillez confirmer la suppression");
        return ok;
    }


    $(document).ready(function () {
        var ctx = "${pageContext.request.contextPath}";
        $.ajax({
            url: ctx + "/api/doesitlikeit",
            dataType: "json",
            data: {
                reviewid: ${review.id}
            }
        }).done(function (doesItLikeIt) {

            if (doesItLikeIt) {
                $(".glyphicon-heart").css("color", "red");
                $(".likeText").text("disliker cette review ");
            } else {
                $(".glyphicon-heart").css("color", "");
                $(".likeText").text("liker cette review ");
            }

        });


        $(".like").click(function () {
            $.ajax({
                url: ctx + "/api/updatelike",
                dataType: "json",
                data: {
                    reviewid: ${review.id}
                }
            }).done(function (addLike) {

                var $counter = $(".counter").text();
                parseInt($counter);

                if (addLike) {
                    $counter++;
                    $(".glyphicon-heart").css("color", "red");
                    $(".likeText").text("disliker cette review ");
                } else {
                    $counter--;
                    $(".glyphicon-heart").css("color", "");
                    $(".likeText").text("liker cette review ");
                }
                $(".counter").text($counter);
            });

        });

    });


</script>
</body>
</html>
