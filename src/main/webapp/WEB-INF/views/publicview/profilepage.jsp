
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>page de profil</title>
        <c:import url="../includes/bootstrap.jsp" />
    </head>
    <body>
        <c:import url="../includes/navbar.jsp" />
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1>page de ${user.username}</h1>

                <div class="well">
                    <div class="text-center"><img width="200" class="img-thumbnail" src="<c:url value="/download/profilepic/${user.username}"/>" /></div>
                    <p>${user.country}, ${user.city}</p>
                    <sec:authorize access="isAnonymous()" >
                        <p><a href="<c:url value="/signup" />">inscrivez-vous</a> pour lui envoyer un message</p>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <a href="<c:url value="/user/privatemessage/new/${user.username}" />"><button type="button" class="btn btn-primary">envoyer un message privé</button></a>
                    </sec:authorize>
                    <h3>présentation</h3>
                    <p>${user.description}</p>
                    <a href="<c:url value="/reviewsbyauthor/${user.username}" />"><button class="btn btn-default">voir reviews</button></a>
                    <a href="<c:url value="/commentsbyauthor/${user.username}" />"><button class="btn btn-default">voir commentaires</button></a>
                    <a href="<c:url value="/likedreviews/${user.username}" />"><button class="btn btn-default">voir reviews likées</button></a>

                </div>
                
            </div>
        </div>
        
        
        
        
    </body>
</html>
