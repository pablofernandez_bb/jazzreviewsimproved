
package com.jazzreviews.service.impl;

import com.jazzreviews.entity.Comm;
import com.jazzreviews.repository.CommentRepository;
import com.jazzreviews.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class CommentServiceImpl implements CommentService{

    @Autowired
    CommentRepository commentRepository;
    
    @Override
    public Comm saveComment(Comm c) {
                
        return commentRepository.save(c);
    }

    @Override
    public void deleteComment(Long id) {
        commentRepository.delete(id);
    }

}
