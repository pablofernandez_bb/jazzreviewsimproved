package com.jazzreviews.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "comm")
public class Comm implements Serializable, Comparable<Comm> {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private User author;
    private String content;
    @ManyToOne
    private Review review;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    @Override
    public int compareTo(Comm o) {
        return getCreationDate().compareTo(o.getCreationDate());
    }
}
