package com.jazzreviews.service.impl;

import com.jazzreviews.entity.SubjectAnswer;
import com.jazzreviews.repository.SubjectAnswerRepository;
import com.jazzreviews.service.SubjectAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class SubjectAnswerServiceImpl implements SubjectAnswerService {

    @Autowired
    SubjectAnswerRepository subjectAnswerRepository;

    @Override
    public void saveSubjectAnswer(SubjectAnswer s) {
        subjectAnswerRepository.save(s);
    }
}
