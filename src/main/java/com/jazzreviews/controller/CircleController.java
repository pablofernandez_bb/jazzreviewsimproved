package com.jazzreviews.controller;

import com.jazzreviews.entity.*;
import com.jazzreviews.exception.PageNotFoundException;
import com.jazzreviews.service.CircleService;
import com.jazzreviews.service.SubjectAnswerService;
import com.jazzreviews.service.SubjectService;
import com.jazzreviews.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.Date;

@Controller
@RequestMapping("/user/circle")
public class CircleController {

    @Autowired
    UserService userService;

    @Autowired
    CircleService circleService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    SubjectAnswerService subjectAnswerService;

    @RequestMapping("/createcirclepage")
    public String createCirclePage() {

        return "circleview/createcirclepage";
    }

    @RequestMapping("/circlemainpage/{id}")
    public String circleMainPage(@AuthenticationPrincipal User user, @PathVariable Long id, Model model) {

        Circle currentCircle = circleService.findById(id);

        User currentUser = userService.findById(user.getId());

        boolean isCircleMember = false;
        for (User u : currentCircle.getMembers()) {
            if (u.getUsername().equals(currentUser.getUsername())) {
                isCircleMember = true;
                break;
            }
        }

        Authority roleAdmin = userService.createOrGetAuthority("ROLE_ADMIN");
        if (user.getAuthorities().contains(roleAdmin)) {
            isCircleMember = true;
        }

        if (!isCircleMember) {
            return "publicview/forbiddenpage";
        }

        boolean isAdmin = currentCircle.getCircleAdmin().equals(user);

        model.addAttribute("isAdmin",isAdmin);
        model.addAttribute("circle", currentCircle);


        return "circleview/circlemainpage";
    }

    @RequestMapping("/createcircle")
    public String createCircle(@AuthenticationPrincipal User user, @RequestParam String groupname, @RequestParam String description, @RequestParam String members) {

        Circle currentCircle = new Circle();
        currentCircle.setCircleName(groupname);
        currentCircle.setDescription(description);

        User admin = userService.findById(user.getId());

        currentCircle.setCircleAdmin(admin);

        String membersWithoutTheEnd = members.substring(0, members.lastIndexOf(","));

        String[] arr = membersWithoutTheEnd.split(",\\s+");

        try {
            for (String member : arr) {
                User u = userService.findByUsername(member);
                if (u != null) {
                    currentCircle.getMembers().add(u);
                    u.getBelongingCircles().add(currentCircle);
                }
            }
        } catch (Exception e) {

        }

        currentCircle.getMembers().add(userService.findById(user.getId()));
        Circle newCircle = circleService.saveCircle(currentCircle);

        return "redirect:/user/circle/circlemainpage/" + newCircle.getId();
    }

    @RequestMapping("/addmember")
    public String addMember(@AuthenticationPrincipal User user, @RequestParam String memberName, @RequestParam Long circleid) {

        User mainUser = userService.findById(user.getId());
        User userToAdd = userService.findByUsername(memberName);
        Circle foundCircle = circleService.findById(circleid);

        if (userToAdd == null) {
            return "redirect:/user/circle/circlemainpage/" + foundCircle.getId();
        }

        boolean isCircleMember = false;
        for (User u : foundCircle.getMembers()) {
            if (u.getUsername().equals(mainUser.getUsername())) {
                isCircleMember = true;
                break;
            }
        }

        if (!isCircleMember) {
            return "publicview/forbiddenpage";
        }

        foundCircle.getMembers().add(userToAdd);
        circleService.saveCircle(foundCircle);

        return "redirect:/user/circle/circlemainpage/" + foundCircle.getId();
    }

    @RequestMapping("/postsubject")
    public String postSubject(@AuthenticationPrincipal User user, @RequestParam String content, @RequestParam Long circleid) {

        Subject newSubject = new Subject();
        newSubject.setContent(content);
        newSubject.setAuthor(user);
        newSubject.setCreationDate(new Date());
        newSubject.setOwningCircle(circleService.findById(circleid));

        subjectService.saveSubject(newSubject);

        return "redirect:/user/circle/circlemainpage/" + circleid;
    }

    @RequestMapping("/postanswer")
    public String postAnswer(@AuthenticationPrincipal User user, @RequestParam Long subjectid, @RequestParam String answer, @RequestParam Long circleid) {

        Subject mainSubject = subjectService.findById(subjectid);
        SubjectAnswer currentAnswer = new SubjectAnswer();

        currentAnswer.setAuthor(user);
        currentAnswer.setContent(answer);
        currentAnswer.setOwningSubject(mainSubject);
        currentAnswer.setCreationDate(new Date());

        subjectAnswerService.saveSubjectAnswer(currentAnswer);

        mainSubject.getAnswers().add(currentAnswer);

        subjectService.saveSubject(mainSubject);

        return "redirect:/user/circle/circlemainpage/" + circleid;

    }

    @RequestMapping("/removememberpage/{circleid}")
    public String removeMemberPage(@AuthenticationPrincipal User user, @PathVariable Long circleid, Model model) {

        Circle foundCircle = circleService.findById(circleid);

        if (! foundCircle.getCircleAdmin().getUsername().equals(user.getUsername()))
            return "publicview/forbiddenpage";

        foundCircle.getMembers().remove(user);
        model.addAttribute("circle",foundCircle);

        return "circleview/removememberpage";
    }

    @RequestMapping("/removemember/{userid}/{circleid}")
    public String removeMember(@AuthenticationPrincipal User user, @PathVariable Long userid, @PathVariable Long circleid) {

        Circle foundCircle = circleService.findById(circleid);

        if (! foundCircle.getCircleAdmin().getUsername().equals(user.getUsername())) {
            return "publicview/forbiddenpage";
        }

        User memberToRemove = userService.findById(userid);


        foundCircle.getMembers().remove(memberToRemove);
        circleService.saveCircle(foundCircle);

        return "redirect:/user/circle/circlemainpage/" + circleid;
    }

    @RequestMapping("/deletecircle/{circleid}")
    public String deleteCircle(@PathVariable Long circleid, @AuthenticationPrincipal User user) {

        Circle foundCircle = circleService.findById(circleid);

        if (foundCircle == null) {
            return "publicview/errorpage";
        }

        if (! foundCircle.getCircleAdmin().getUsername().equals(user.getUsername())) {
            return "publicview/forbiddenpage";
        }

        circleService.deleteCircle(foundCircle);

        return "redirect:/user/dashboard";

    }

    @RequestMapping("/leavecircle/{circleid}")
    public String leaveCircle(@PathVariable Long circleid, @AuthenticationPrincipal User user) {

        Circle foundCircle = circleService.findById(circleid);

        foundCircle.getMembers().remove(user);

        circleService.saveCircle(foundCircle);

        return "redirect:/user/dashboard";

    }

    @ExceptionHandler(Exception.class)
    public String handleGeneralException(HttpServletRequest req, Exception e, Model model) throws Exception {

        model.addAttribute("exception",e);
        model.addAttribute("url",req.getRequestURL());

        return "publicview/errorpage";
    }


}
