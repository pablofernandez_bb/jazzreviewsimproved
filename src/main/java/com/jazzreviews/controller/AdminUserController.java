package com.jazzreviews.controller;


import com.jazzreviews.entity.*;
import com.jazzreviews.form.AdminSignUpForm;
import com.jazzreviews.service.CircleService;
import com.jazzreviews.service.CommentService;
import com.jazzreviews.service.ReviewService;
import com.jazzreviews.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.SQLException;
import java.util.HashSet;

@Controller
@RequestMapping("/admin")
public class AdminUserController {

    @Autowired
    UserService userService;

    @Autowired
    ReviewService reviewService;

    @Autowired
    CommentService commentService;

    @Autowired
    CircleService circleService;

    @RequestMapping("/adminconsole")
    public String adminconsole(Model model) {

        model.addAttribute("usersList", userService.findAll());


        return "adminview/adminconsole";
    }

    @RequestMapping(value = "/supercreate", method = RequestMethod.GET)
    public String superCreate(Model model) {
        model.addAttribute("adminSignUpForm", new AdminSignUpForm());

        return "adminview/supercreate";
    }

    @RequestMapping(value = "/supercreate", method = RequestMethod.POST)
    public String processSuperCreate(@Valid AdminSignUpForm adminSignUpForm, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {

            return "adminview/supercreate";

        } else {

            User user = adminSignUpForm.toUser();

            if (adminSignUpForm.getRole().equals("user")) {
                Authority role = userService.createOrGetAuthority("ROLE_USER");
                user.getAuthorities().add(role);
            }

            if (adminSignUpForm.getRole().equals("admin")) {
                Authority role = userService.createOrGetAuthority("ROLE_USER");
                Authority role2 = userService.createOrGetAuthority("ROLE_ADMIN");
                user.getAuthorities().add(role);
                user.getAuthorities().add(role2);
            }

            userService.signUp(user);

            return "redirect:/admin/adminconsole";
        }
    }

    @RequestMapping("/deletereview/{reviewid}")
    public String deleteReview(@PathVariable Long reviewid) {

        Review revToKill = reviewService.findById(reviewid);

        for(Comm c : revToKill.getComments()) {
            commentService.deleteComment(c.getId());
        }

        reviewService.deleteReview(revToKill.getId());

        return "redirect:/allreviews";
    }

    @RequestMapping("/deletecomm/{reviewid}/{id}")
    public String deleteComm(@PathVariable Long id, @PathVariable Long reviewid) {
        commentService.deleteComment(id);

        return "redirect:/reviewpage/" + reviewid;
    }

    @RequestMapping(value = "/editmember/{id}", method = RequestMethod.GET)
    public String editMemberPage(@PathVariable Long id, Model model) {

        User foundUser = userService.findById(id);
        model.addAttribute("user",foundUser);
        Authority roleAdmin = userService.createOrGetAuthority("ROLE_ADMIN");
        model.addAttribute("isAdmin",foundUser.getAuthorities().contains(roleAdmin));

        return "adminview/editmember";
    }


    @RequestMapping(value = "/editmember/{id}", method = RequestMethod.POST)
    public String editMember(@PathVariable Long id, @RequestParam String username, @RequestParam String email, @RequestParam String firstname, @RequestParam String lastname, @RequestParam String country,
                             @RequestParam String city, @RequestParam String role) {

        User u = userService.findById(id);

        u.setUsername(username);
        u.setEmail(email);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setCountry(country);
        u.setCity(city);

        Authority roleAdmin = userService.createOrGetAuthority("ROLE_ADMIN");
        Authority roleUser = userService.createOrGetAuthority("ROLE_USER");

        if (role.equals("admin")) {
            boolean isUser = true;
            for(Authority a : u.getAuthorities()) {
                if (a.getAuthority().equals("ROLE_ADMIN")) {
                    isUser = false;
                }
            }
            if (isUser) {
                u.getAuthorities().add(roleAdmin);
            }
        }

        if (role.equals("user")) {
            boolean isAdmin = false;
            for (Authority a : u.getAuthorities()) {
                if (a.getAuthority().equals("ROLE_ADMIN")) {
                    isAdmin = true;
                }
            }
            if (isAdmin) {
                u.setAuthorities(new HashSet<Authority>());
                u.getAuthorities().add(roleUser);
            }
        }

        userService.updateUser(u);

        return "redirect:/admin/adminconsole";
    }


    @RequestMapping("/disableuser/{id}")
    public String disableUser(@PathVariable Long id) {

        User futureDead = userService.findById(id);

        futureDead.setEnabled(!futureDead.isEnabled());

        userService.updateUser(futureDead);

        return "redirect:/admin/adminconsole";
    }


    @RequestMapping("/seeallcircles")
    public String seeAllCircles(Model model) {

        model.addAttribute("circles",circleService.findAll());

        return "adminview/seeallcircles";
    }

    @RequestMapping("/deletecircle/{circleid}")
    public String deleteCircle(@PathVariable Long circleid) {

        Circle foundCircle = circleService.findById(circleid);

        if (foundCircle == null) {
            return "publicview/errorpage";
        }

        circleService.deleteCircle(foundCircle);

        return "redirect:/admin/seeallcircles";

    }

    @RequestMapping("/accesscircle/{circleid}")
    public String accessGroup(@PathVariable Long circleid) {

        return "redirect:/user/circle/circlemainpage/" + circleid;
    }


    @ExceptionHandler(Exception.class)
    public String handleGeneralException(HttpServletRequest req, Exception e, Model model) throws Exception {

        model.addAttribute("exception",e);
        model.addAttribute("url",req.getRequestURL());

        return "publicview/errorpage";
    }

}
