
package com.jazzreviews.service;

import com.jazzreviews.entity.Authority;
import com.jazzreviews.entity.User;

import java.util.List;
import java.util.Set;


public interface UserService {
    
    User findByUsername(String username);
    
    Authority createOrGetAuthority(String authority);
    
    User signUp(User user);

    void initDB();

    void updateUser(User u);

    User findById(Long id);

    List<User> findAll();

    int computeLikes(User user);

    List<User> findByUsernameLike(String name_startsWith);
}
