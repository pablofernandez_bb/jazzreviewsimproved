<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>tous les cercles</title>
    <c:import url="../includes/bootstrap.jsp"/>
</head>
<body>
<c:import url="../includes/navbar.jsp"/>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="text-center">tous les cercles</h1>

        <div class="list-group">
            <c:forEach var="circle" items="${circles}">
                <div class="col-md-9">
                    <a href="<c:url value="/admin/accesscircle/${circle.id}"/>"
                       class="list-group-item">${circle.circleName}</a></div>
                <div class="col-md-3">
                    <a href="<c:url value="/admin/deletecircle/${circle.id}"/>">
                        <button onclick="return areYouSure()" class="btn btn-danger">supprimer cercle</button>
                    </a>
                </div>
            </c:forEach>
        </div>
    </div>


</div>
<script>
    function areYouSure() {
        var ok = confirm("veuillez confirmer la suppression");
        return ok;
    }
</script>
</body>
</html>