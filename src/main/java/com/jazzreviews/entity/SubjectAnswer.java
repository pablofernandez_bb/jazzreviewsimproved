package com.jazzreviews.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "subjectanswer")
public class SubjectAnswer implements Serializable{

    @Id
    @GeneratedValue
    private Long id;
    @Column(length = 200000)
    private User author;
    @Column(length = 20000)
    private String content;
    @ManyToOne
    private Subject owningSubject;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Subject getOwningSubject() {
        return owningSubject;
    }

    public void setOwningSubject(Subject owningSubject) {
        this.owningSubject = owningSubject;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
