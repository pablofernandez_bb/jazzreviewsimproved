<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${circle.circleName}</title>
    <c:import url="../includes/bootstrap.jsp"/>
</head>
<body>
<c:import url="../includes/navbar.jsp"/>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="row">
            <h1 class="class-md-3">${circle.circleName}</h1>
            <div class="class-md-3 pull-right">
                <c:if test="${isAdmin}">
                    <a href="<c:url value="/user/circle/removememberpage/${circle.id}"/>">
                        <button class="btn btn-primary">retirer un membre</button>
                    </a>

                    <a href="<c:url value="/user/circle/deletecircle/${circle.id}"/>">
                        <button onclick="return areYouSure()" class="btn btn-danger">supprimer groupe</button>
                    </a>

                </c:if>

                <c:if test="${! isAdmin}">
                    <a href="<c:url value="/user/circle/leavecircle/${circle.id}"/>">
                        <button onclick="return areYouSure()" class="btn btn-danger">quitter le groupe</button>
                    </a>
                </c:if>
            </div>
        </div>
        <p><strong>admin :</strong>
            <a
                    href="<c:url value="/profilepage/${circle.circleAdmin}" />">${circle.circleAdmin}
            </a>
        </p>
        <p><strong>membres :</strong></p>
        <ul>
            <c:forEach items="${circle.members}" var="member">
                <li><a href="<c:url value="/profilepage/${member.username}" />">${member.username}</a></li>
            </c:forEach>
        </ul>

        <div class="pull-right">
            <div class="pull-right">ajouter un membre
                <button class="btn btn-primary addMember">+</button>
            </div>
            <div class="addMemberForm" style="visibility: hidden">
                <form method="post" action="<c:url value="/user/circle/addmember"/>">
                    <input class="form-control memberfield" type="text" name="memberName"/>
                    <input type="hidden" name="circleid" value="${circle.id}"/>
                    <input class="btn btn-default pull-right" type="submit" value="go"/>
                </form>
            </div>
        </div>
        <p><strong>description : </strong>${circle.description}</p>

        <form method="post" action="<c:url value="/user/circle/postsubject" />">
            <div class="form-group">
                <textarea class="form-control" rows="8" id="comment" name="content"
                          placeholder="nouveau sujet..."></textarea>
            </div>
            <input type="hidden" value="${circle.id}" name="circleid"/>
            <button type="submit" class="btn btn-default">envoyer</button>
        </form>
        <p></p>
        <c:forEach items="${circle.subjects}" var="subject">
            <div class="well">
                <p><a href="<c:url value="/profilepage/${subject.author}" />"><strong>${subject.author}</strong></a>
                    <em><fmt:formatDate value="${subject.creationDate}" pattern="dd-MM-yyyy"/> à <fmt:formatDate
                            value="${subject.creationDate}" pattern="HH:mm"/> </em></p>
                <p>${subject.content}</p>
                <c:forEach items="${subject.answers}" var="answer">
                    <div class="well answer">
                        <p>
                            <a href="<c:url value="/profilepage/${answer.author}" />"><strong>${answer.author}</strong></a>
                            <em><fmt:formatDate value="${answer.creationDate}" pattern="dd-MM-yyyy"/> à <fmt:formatDate
                                    value="${answer.creationDate}" pattern="HH:mm"/> </em></p>

                        <p>${answer.content}</p>
                    </div>
                </c:forEach>
                <form method="post" action="<c:url value="/user/circle/postanswer" />">
                    <div class="form-group">
                        <textarea class="form-control" rows="8" id="answer" name="answer"
                                  placeholder="répondre..."></textarea>
                    </div>
                    <input type="hidden" value="${circle.id}" name="circleid"/>
                    <input type="hidden" value="${subject.id}" name="subjectid"/>
                    <button type="submit" class="btn btn-default">envoyer</button>
                </form>
            </div>
        </c:forEach>

    </div>


</div>
<script>

    function areYouSure() {
        var ok = confirm("veuillez confirmer la suppression");
        return ok;
    }

    $(document).ready(function () {
        $(".addMember").click(function () {
            $(".addMemberForm").css("visibility", "visible");
            $(".memberfield").focus();
        });
    });

    $('.memberfield').autocomplete({
        source: function (requete, reponse) {
            $.ajax({
                url: '<c:url value="/api/getusernames" />',
                dataType: 'json',
                data: {
                    name_startsWith: $('.memberfield').val(),
                    maxRows: 15
                },

                success: function (data) {
                    reponse(data, function (objet) {
                        return objet;
                    });
                }
            });
        }
    });
</script>
</body>
</html>