<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Catégorie : ${category}</title>
    <c:import url="../includes/bootstrap.jsp"/>
</head>
<body>
<c:import url="../includes/navbar.jsp"/>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="text-center">Catégorie : ${category}</h1>
<br/>
        <c:forEach items="${reviews}" var="review">
            <a href="<c:url value="/reviewpage/${review.id}"/>">
                <div class="blocReview well col-md-3 col-md-offset-1">

                    <div class="img-mini img-thumbnail"
                         style="background-image: url('<c:url value="/download/reviewpic/${review.id}"/>');"></div>
                    <h3 class="text-center">${review.title}</h3>

                </div>
            </a>
        </c:forEach>
    </div>


</div>

</body>
</html>