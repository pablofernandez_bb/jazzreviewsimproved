
package com.jazzreviews.entity;

import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

@Entity
@Table(name = "review")
public class Review implements Serializable, Comparable<Review> {

    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    private User author;
    private String title;
    @ManyToOne
    private Category category;
    @Column(length = 20000)
    private String content;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Value(value = "0")
    private int likes;
    private String picturePath;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "review")
    @OrderBy(value = "creationDate")
    private List<Comm> comments;
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<User> likingUsers;


    public Set<User> getLikingUsers() {
        if (likingUsers == null)
            likingUsers = new HashSet<>();
        return likingUsers;
    }

    public void setLikingUsers(Set<User> likingUsers) {
        this.likingUsers = likingUsers;
    }

    public Long getId() {
        return id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<Comm> getComments() {

        if (comments == null) {
            comments = new ArrayList<>();
        }
        return comments;
    }

    public void setComments(List<Comm> comments) {
        this.comments = comments;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    @Override
    public int compareTo(Review o) {
        return o.getCreationDate().compareTo(this.getCreationDate());
    }
}
