package com.jazzreviews.controller;


import com.jazzreviews.entity.PrivateMessage;
import com.jazzreviews.entity.User;
import com.jazzreviews.service.PrivateMessageService;
import com.jazzreviews.service.UserService;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.Date;
import java.util.TreeSet;

@Controller
@RequestMapping("user/privatemessage")
public class PrivateMessageController {

    @Autowired
    UserService userService;

    @Autowired
    PrivateMessageService privateMessageService;

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newMessage() {
        return "privatemessageview/newprivatemessage";
    }

    @RequestMapping(value = "/new/{destinationUser}", method = RequestMethod.GET)
    public String newMessageFromProfilePage(@PathVariable String destinationUser, Model model) {

        model.addAttribute("destinationUser",destinationUser);

        return "privatemessageview/newprivatemessage";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String sendNewMessage(@AuthenticationPrincipal User user, @RequestParam String destinationUser, @RequestParam String messageBody, Model model) {

        User author = userService.findByUsername(user.getUsername());

        User destinationUserFound = userService.findByUsername(destinationUser);

        PrivateMessage pm = new PrivateMessage();
        pm.setAuthor(author);
        pm.setDestinationUser(destinationUserFound);
        pm.setContent(messageBody);

        if (messageBody.length() >= 80) {
            String exc = Jsoup.parse(messageBody).text();
            pm.setExcerpt(exc.substring(0,79) + "...");
        } else {
            pm.setExcerpt(Jsoup.parse(messageBody).text());
        }

        pm.setSendingTime(new Date());

        destinationUserFound.setMessageUnread(true);

        userService.updateUser(destinationUserFound);

        privateMessageService.savePrivateMessage(pm);

        String msg = "";
        if (destinationUserFound == null) {
            msg = "failed";
        } else {
            msg = "success";
        }

        model.addAttribute("msg",msg);

        return "privatemessageview/newprivatemessage";

    }

    @RequestMapping("/inbox")
    public String inbox(@AuthenticationPrincipal User user, Model model) {

        User currentUser = userService.findById(user.getId());

        currentUser.setMessageUnread(false);

        userService.updateUser(currentUser);

        TreeSet<PrivateMessage> pms = new TreeSet<>(currentUser.getPrivateMessages());

        model.addAttribute("user",currentUser);
        model.addAttribute("pms",pms);

        return "privatemessageview/inbox";
    }

    @RequestMapping("/wholemessage/{messageid}")
    public String wholeMessage(@PathVariable Long messageid, Model model){

        model.addAttribute("privateMessage", privateMessageService.findById(messageid));

        return "privatemessageview/wholemessage";
    }

    @ExceptionHandler(Exception.class)
    public String handleGeneralException(HttpServletRequest req, Exception e, Model model) throws Exception {

        model.addAttribute("exception",e);
        model.addAttribute("url",req.getRequestURL());

        return "publicview/errorpage";
    }



}
