
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search</title>
        <c:import url="../includes/bootstrap.jsp" />
    </head>
    <c:import url="../includes/navbar.jsp" />
    <h1>résultats de recherche</h1>
    
    <c:forEach items="${foundReviews}" var="review" >
        <div class="well">
            <a href="<c:url value="/reviewpage/${review.id}" />">
                <h2>${review.title}</h2>
            </a>
                <p><strong><a href="<c:url value="/profilepage/${review.author}"/>">${review.author}</a></strong></p>
                <p><fmt:formatDate value="${review.creationDate}" pattern="dd-MM-yyyy"/></p>
            
        </div>
    </c:forEach>
    
</body>
</html>
