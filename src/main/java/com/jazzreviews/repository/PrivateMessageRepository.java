package com.jazzreviews.repository;


import com.jazzreviews.entity.PrivateMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrivateMessageRepository extends JpaRepository<PrivateMessage,Long> {

    PrivateMessage findById(Long messageid);
}
