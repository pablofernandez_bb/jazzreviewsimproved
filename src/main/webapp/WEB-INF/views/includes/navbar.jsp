<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/" /> "><span class="mainTitle">JAZZ REVIEWS </span>  <img class="saxnav" src="<c:url value="/resources/img/saxophonenav.png"/>" /></a>
        </div>
        <ul class="nav navbar-nav">
            <li class="clickable"><a href="<c:url value="/" /> ">Home</a></li>
            <sec:authorize access="isAuthenticated()">
                <li class="clickable"><a href="<c:url value="/user/dashboard" /> ">Dashboard</a></li>

            </sec:authorize>
            <li class="clickable"><a href="<c:url value="/allreviews" /> ">Toutes les reviews</a></li>
            <li class="clickable"><a href="<c:url value="/topreviews"/>">Top reviews</a></li>
            <li class="clickable"><a href="<c:url value="/categories"/>">Cat&eacute;gories</a></li>
        </ul>
        <div class="col-sm-3 col-md-2">
            <form class="navbar-form" role="search" method="post" action="<c:url value="/search" />">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Chercher..." name="searchQuery">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <sec:authorize access="isAnonymous()">

                <li><a href="<c:url value="/signup" /> "><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="<c:url value="/login" />"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>

            </sec:authorize>
            <sec:authorize access="isAuthenticated()">

                <sec:authorize access="hasRole('ROLE_ADMIN')" >
                    <li><a href="<c:url value="/admin/adminconsole" />"><span class="glyphicon glyphicon-cog"></span> console admin</a></li>
                </sec:authorize>
                <sec:authentication property="principal.username" var="username"></sec:authentication>
                <li>
                    <script>
                        var ctx = "${pageContext.request.contextPath}";
                        $(document).ready(function() {
                            $.ajax({
                                url : ctx + "/api/checkmessages",
                                data : "username=${username}"
                            }).done(function(hasNewMessage) {
                                if (hasNewMessage) {
                                    $(".newMsg").addClass("label label-default").text("new");
                                } else {
                                    $(".newMsg").removeClass("label label-default").text("");
                                }
                            });
                        });
                    </script>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-envelope"></span> Messages
                            <span class="newMsg"></span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="<c:url value="/user/privatemessage/new" />">nouveau message</a></li>
                            <li><a href="<c:url value="/user/privatemessage/inbox" />">boite de r&eacute;ception</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="<c:url value="/profilepage/${username}" /> "><span
                        class="glyphicon glyphicon-user"></span> ${username} </a></li>
                <li><a href="<c:url value="/logout" />"><span class="glyphicon glyphicon-log-out"></span> logout</a>
                </li>
            </sec:authorize>
        </ul>
    </div>
    <script>
        $(document).ready(function(){
            $(".clickable").click(function(){
                $(this).toggleClass("active");
            });
        });
    </script>
</nav>