
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>top reviews</title>
    <c:import url="../includes/bootstrap.jsp" />
</head>
<body>
<c:import url="../includes/navbar.jsp" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="text-center">reviews les plus likées</h1>
        <br/>
        <c:forEach items="${reviews}" var="review">

            <div class="col-sm-6 col-md-4">
                <a href="<c:url value="/reviewpage/${review.id}"/>">
                    <div class="thumbnail thumbnail-me">
                        <div class="img-mini img-thumbnail"
                             style="background-image: url('<c:url value="/download/reviewpic/${review.id}"/>');">
                        </div>
                        <div class="caption">
                            <h4 class="text-center">${review.title}</h4>
                            <p class="text-center">catégorie : <strong><a href="<c:url value="/showcategory/${review.category.name}"/>">${review.category.name}</a></strong></p>
                            <p class="text-center">auteur : <strong><a href="<c:url value="/profilepage/${review.author}"/>">${review.author}</a></strong></p>
                            <p class="text-center"><span class="glyphicon glyphicon-heart"></span> ${review.likes}</p>
                        </div>
                    </div>
                </a>
            </div>

        </c:forEach>
    </div>


</div>

</body>
</html>