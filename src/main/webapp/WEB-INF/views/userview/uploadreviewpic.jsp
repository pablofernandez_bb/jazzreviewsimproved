
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>upload image</title>
    <c:import url="../includes/bootstrap.jsp" />
</head>
<body>
<c:import url="../includes/navbar.jsp" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-6">

        <c:url var="addUrl" value="/user/upload/reviewpic"/>
        <form:form method="post" action="${addUrl}" enctype="multipart/form-data" modelAttribute="fileModel">
            <div class="form-group" >
                <input name="reviewid" type="hidden" class="form-control" value="${reviewid}" />
                <form:input path="file" type="file" class="form-control" accept="image/*" />

                <input type="submit" value="upload" class="btn btn-primary" />
            </div>
        </form:form>
        <div class="alert alert-success">la review a été sauvée, uploadez une image si vous le souhaitez</div>

    </div>


</div>

</body>
</html>