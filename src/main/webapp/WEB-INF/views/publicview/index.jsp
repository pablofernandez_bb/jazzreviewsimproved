<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE HTML>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Jazz Reviews</title>
    <c:import url="../includes/bootstrap.jsp"/>

</head>

<c:import url="../includes/navbar.jsp"/>
<body>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="text-center">Bienvenue</h1>
        <br/>
        <br/>
        <sec:authorize access="isAnonymous()">
            <p>pour participer à cette communauté de ouf-guedin, <a href="<c:url value="/signup"/>">inscrivez-vous</a> ou <a href="<c:url value="/login"/>">identifiez-vous</a></p>
        </sec:authorize>

        <h4>Tout chaud sorti du four</h4>

        <c:forEach items="${reviews}" var="review">

                <div class="col-sm-6 col-md-4">
                    <a href="<c:url value="/reviewpage/${review.id}"/>">
                    <div class="thumbnail thumbnail-me">
                        <div class="img-mini img-thumbnail"
                             style="background-image: url('<c:url value="/download/reviewpic/${review.id}"/>');">
                        </div>
                        <div class="caption">
                            <h4 class="text-center">${review.title}</h4>
                            <p class="text-center">catégorie : <strong><a href="<c:url value="/showcategory/${review.category.name}"/>">${review.category.name}</a></strong></p>
                            <p class="text-center">auteur : <strong><a href="<c:url value="/profilepage/${review.author}"/>">${review.author}</a></strong></p>
                        </div>
                    </div>
                    </a>
                </div>


        </c:forEach>
    </div>

</div>
<script>






</script>
</body>
</html>
