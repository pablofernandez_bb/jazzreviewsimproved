package com.jazzreviews.form;


import com.jazzreviews.entity.User;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;


public class AdminSignUpForm {


    @Size(min = 4, max = 24, message = "un username est requis et doit être entre 4 et 24 caractères!")
    @Pattern(regexp = "[\\w&&\\S]*", message = "format du username incorrect")
    private String username;

    @Size(min = 4, max = 24, message = "le password doit faire entre 4 et 24 caractères!")
    @Pattern(regexp = "[\\w&&\\S]*", message = "password format incorrect")
    private String password;

    @Size(min = 4, max = 24, message = "le password doit faire entre 4 et 24 char!")
    @Pattern(regexp = "[\\w&&\\S]*", message = "password format incorrect")
    private String passwordCheck;

    @Size(min = 1, max = 255, message = "le prénom doit faire entre 1 et 255 char!")
    private String firstName;

    @Size(min = 1, max = 255, message = "le nom de famille doit faire entre 1 et 255 char!")
    private String lastName;

    @Pattern(regexp = "^[a-z]+@[a-z]+\\.[a-z]+",
            message = "email non conforme")
    private String email;

    @Past(message = "must be in the past")
    @NotNull(message = "date can not be null!")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateOfBirth;

    @Size(min = 1, max = 50, message = "doit etre entre 1 et 50")
    private String city;

    @Size(min = 1, max = 50, message = "doit etre entre 1 et 50")
    private String country;

    private String role;


    @AssertTrue(message = "passwords dont match")
    public boolean isPasswordsMatch() {
        if (password == null || passwordCheck == null) {
            return false;
        }
        return password.equals(passwordCheck);
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordCheck() {
        return passwordCheck;
    }

    public void setPasswordCheck(String passwordCheck) {
        this.passwordCheck = passwordCheck;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public User toUser(){

        User u = new User();

        u.setUsername(getUsername());
        u.setPassword(getPassword());
        u.setFirstName(getFirstName());
        u.setLastName(getLastName());
        u.setEmail(getEmail());
        u.setDateOfBirth(getDateOfBirth());
        u.setCity(getCity());
        u.setCountry(getCountry());

        return u;

    }



}
