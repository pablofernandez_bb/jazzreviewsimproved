package com.jazzreviews.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "subject")
public class Subject implements Serializable, Comparable<Subject> {

    @Id
    @GeneratedValue
    private Long id;
    @Column(length = 200000)
    private User author;
    @Column(length = 20000)
    private String content;
    @ManyToOne
    private Circle owningCircle;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "owningSubject")
    @OrderBy(value = "creationDate")
    private List<SubjectAnswer> answers;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Circle getOwningCircle() {
        return owningCircle;
    }

    public void setOwningCircle(Circle owningCircle) {
        this.owningCircle = owningCircle;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<SubjectAnswer> getAnswers() {
        if (answers == null) {
            answers = new ArrayList<>();
        }
        return answers;
    }

    public void setAnswers(List<SubjectAnswer> answers) {
        this.answers = answers;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    @Override
    public int compareTo(Subject o) {
        return o.getCreationDate().compareTo(this.getCreationDate());
    }

}
