package com.jazzreviews.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "circle")
public class Circle implements Serializable{

    @Id
    @GeneratedValue
    private Long id;
    private String circleName;
    @Column(length = 200000)
    private User circleAdmin;
    private String description;
    @OneToMany(mappedBy = "owningCircle", fetch = FetchType.EAGER)
    @OrderBy(value = "creationDate DESC")
    private List<Subject> subjects;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<User> members;

    public List<Subject> getSubjects() {

        if (subjects == null) {
            subjects = new ArrayList<>();
        }
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getCircleAdmin() {
        return circleAdmin;
    }

    public void setCircleAdmin(User circleAdmin) {
        this.circleAdmin = circleAdmin;
    }

    public Set<User> getMembers() {
        if (members == null) {
            members = new HashSet<>();
        }

        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Circle circle = (Circle) o;

        if (getId() != null ? !getId().equals(circle.getId()) : circle.getId() != null) return false;
        return getCircleName() != null ? getCircleName().equals(circle.getCircleName()) : circle.getCircleName() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getCircleName() != null ? getCircleName().hashCode() : 0);
        return result;
    }
}
