
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sign Up page</title>
        <c:import url="../includes/bootstrap.jsp" />
    </head>
    <c:import url="../includes/navbar.jsp" />
    <body>
        <div class="row">
            <div class="col-md-4"></div>
            <h1 class="col-md-4">créer votre compte</h1>

        </div>
        <p></p>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <c:url var="addUrl" value="/signup"/>
                <form:form method="post" action="${addUrl}" commandName="signUpForm" role="form" >
                    <div class="form-group" >
                        <label for="username">username</label>
                        <form:input path="username" class="form-control first-input"  />
                        <form:errors path="username" />

                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <form:input path="password" class="form-control" type="password" />
                        <form:errors path="password" />

                    </div>

                    <div class="form-group">
                        <label for="passwordCheck">Confimer password</label>
                        <form:input path="passwordCheck" class="form-control" type="password" />
                        <form:errors path="passwordCheck" />
                        <form:errors path="passwordsMatch" />

                    </div>
                    <div class="form-group">
                        <label for="firstName">prénom</label>
                        <form:input path="firstName" class="form-control" />
                        <form:errors path="firstName" />
                    </div>

                    <div class="form-group">
                        <label for="lastName">nom de famille</label>
                        <form:input path="lastName" class="form-control" />
                        <form:errors path="lastName" />
                    </div>


                    <div class="form-group">
                        <label for="email">Email</label>
                        <form:input path="email" class="form-control" />
                        <form:errors path="email" />
                    </div>

                    <div class="form-group">
                        <label for="dateOfBirth">date de naissance</label>
                        <form:input type="date" path="dateOfBirth" class="form-control" />
                        <form:errors path="dateOfBirth" />
                    </div>

                    <div class="form-group">
                        <label for="city">ville</label>
                        <form:input path="city" class="form-control" />
                        <form:errors path="city" />
                    </div>

                    <div class="form-group">
                        <label for="country">pays</label>
                        <form:input path="country" class="form-control" />
                        <form:errors path="country" />
                    </div>

 

                    <div class="text-center"> 
                        <input type="submit" class="btn btn-primary" />
                        <input type="reset" class="btn btn-warning" />
                    </div>
                </form:form>
            </div>
        </div>
    <script>
        $(function(){
            $(".first-input").focus();
        });
    </script>
    </body>
</html>
