package com.jazzreviews.service.impl;

import com.jazzreviews.entity.Subject;
import com.jazzreviews.repository.SubjectRepository;
import com.jazzreviews.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    SubjectRepository subjectRepository;


    @Override
    public void saveSubject(Subject s) {
        subjectRepository.save(s);
    }

    @Override
    public Subject findById(Long subjectid) {
        return subjectRepository.findById(subjectid);
    }
}
