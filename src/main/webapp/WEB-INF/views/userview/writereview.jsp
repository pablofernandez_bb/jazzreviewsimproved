
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>écrire une review</title>
        <c:import url="../includes/bootstrap.jsp" />
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
    </head>
    <body>
        <c:import url="../includes/navbar.jsp" />
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1>écrire une review</h1>
                <br/>
                <form role="form" method="post" action="<c:url value="/user/writereview" />" >

                    <div class="form-group">
                        <label for="title">titre</label>
                        <input class="form-control first-input" id="title" type="text" name="title" required />
                    </div>
                    <div class="form-group" class="col-md-3" >
                        <label for="category">catégorie</label>
                        <select class="form-control" id="category" name="category">
                            <c:forEach items="${categories}" var="category" >
                                <option value="${category.name}">${category.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="comment">Votre review:</label>
                        <textarea class="form-control" rows="8" id="comment" name="review"></textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>
        <script>
            $(function(){
                $(".first-input").focus();
            });
        </script>
    </body>
</html>
