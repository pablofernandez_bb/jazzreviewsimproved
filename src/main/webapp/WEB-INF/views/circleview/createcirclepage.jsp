<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>créer un cercle</title>
    <c:import url="../includes/bootstrap.jsp"/>
</head>
<body>
<c:import url="../includes/navbar.jsp"/>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1>créer un cercle</h1>

        <form id="myForm" role="form" method="post" action="<c:url value="/user/circle/createcircle" />">

            <div class="form-group">
                <label for="groupname">nom du cercle</label>
                <input class="form-control" id="groupname" type="text" name="groupname" required/>
            </div>
            <div class="form-group">
                <label for="comment">description :</label>
                <textarea class="form-control" rows="8" id="comment" name="description"></textarea>
            </div>
            <div class="form-group">
                <label for="#">membres :</label>
                <input class="form-control memberfield" type="text" name="members" required/>
            </div>

            <div class="form-group"><input type="submit" class="btn btn-default" /></div>
        </form>

    </div>


</div>
<script>
    $(function() {
        var availableTags = "";
        var ctx = "${pageContext.request.contextPath}";
        $.ajax({
            url: "<c:url value="/api/getusernames2" />",
            success: function (result) {
                availableTags = result;
            }
        });

        function split( val ) {
            return val.split( /,\s*/ );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }

        $( ".memberfield" )
        // don't navigate away from the field on tab when selecting an item
                .bind( "keydown", function( event ) {
                    if ( event.keyCode === $.ui.keyCode.TAB &&
                            $( this ).autocomplete( "instance" ).menu.active ) {
                        event.preventDefault();
                    }
                })
                .autocomplete({
                    minLength: 0,
                    source: function( request, response ) {
                        // delegate back to autocomplete, but extract the last term
                        response( $.ui.autocomplete.filter(
                                availableTags, extractLast( request.term ) ) );
                    },
                    focus: function() {
                        // prevent value inserted on focus
                        return false;
                    },
                    select: function( event, ui ) {
                        var terms = split( this.value );
                        // remove the current input
                        terms.pop();
                        // add the selected item
                        terms.push( ui.item.value );
                        // add placeholder to get the comma-and-space at the end
                        terms.push( "" );
                        this.value = terms.join( ", " );
                        return false;
                    }
                });
    });


/* ajax qui marche nickel pour un
    var end = $(".memberfield").length;
    $('.memberfield').autocomplete({
        source : function(requete, reponse){
            $.ajax({
                url : 'http://localhost:8080/JazzReviews/api/getusernames',
                dataType : 'json',
                data : {
                    name_startsWith : $('.memberfield').val(),
                    maxRows : 15
                },

                success : function(data){
                    reponse(data, function(objet) {
                        return objet;
                    });
                }
            });
        }
    });
    */

/*
    $('.addOne').on("click",function() {
        $(".memberfield").clone(true,true).insertAfter(".memberfield").val("");
    });

    $("#myForm").submit(function() {
        alert($(".memberfield").val());
    });
*/
</script>

</body>
</html>