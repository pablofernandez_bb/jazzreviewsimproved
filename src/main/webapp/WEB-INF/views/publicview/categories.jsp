<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>catégories</title>
    <c:import url="../includes/bootstrap.jsp"/>
</head>
<body>
<c:import url="../includes/navbar.jsp"/>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="text-center">catégories</h1>
        <br/>
        <br/>
        <div class="row">

            <div class="col-xs-6 col-md-3">
                <a href="<c:url value="/showcategory/Post-bop"/>" class="thumbnail catColor">
                    <div class="img-mini img-thumbnail"
                         style="background-image: url('<c:url value="/resources/img/Post-bop.jpg"/>');">
                    </div>

                    <p class="text-center">Post-bop</p>
                </a>
            </div>

            <div class="col-xs-6 col-md-3">
                <a href="<c:url value="/showcategory/Jazz modal"/>" class="thumbnail catColor">
                    <div class="img-mini img-thumbnail"
                         style="background-image: url('<c:url value="/resources/img/Jazz%20modal.jpg"/>');">
                    </div>

                    <p class="text-center">Jazz modal</p>
                </a>
            </div>

            <div class="col-xs-6 col-md-3">
                <a href="<c:url value="/showcategory/Jazz fusion"/>" class="thumbnail catColor">
                    <div class="img-mini img-thumbnail"
                         style="background-image: url('<c:url value="/resources/img/Jazz%20fusion.jpg"/>');">
                    </div>

                    <p class="text-center">Jazz fusion</p>
                </a>
            </div>

            <div class="col-xs-6 col-md-3">
                <a href="<c:url value="/showcategory/Autre"/>" class="thumbnail catColor">
                    <div class="img-mini img-thumbnail"
                         style="background-image: url('<c:url value="/resources/img/Autre.jpg"/>');">
                    </div>

                    <p class="text-center">Autre</p>
                </a>
            </div>


        </div>
    </div>


</div>

</body>
</html>