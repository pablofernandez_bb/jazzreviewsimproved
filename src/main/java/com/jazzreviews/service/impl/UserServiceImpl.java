package com.jazzreviews.service.impl;

import com.jazzreviews.entity.*;
import com.jazzreviews.repository.*;
import com.jazzreviews.service.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    CircleRepository circleRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public UserDetails loadUserByUsername(String u) throws UsernameNotFoundException {

        return userRepository.findByUsername(u);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Authority createOrGetAuthority(String authority) {
        Authority result = authorityRepository.findByAuthority(authority);

        if (result == null) {
            Authority newAuthority = new Authority();
            newAuthority.setAuthority(authority);
            result = authorityRepository.save(newAuthority);
        }

        return result;
    }

    @Override
    public User signUp(User user) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String hashedPwd = encoder.encode(user.getPassword());
        user.setPassword(hashedPwd);

        //les 4 démons du mal
        user.setAccountNonExpired(true);
        user.setCredentialsNonExpired(true);
        user.setAccountNonLocked(true);
        user.setEnabled(true);

        return userRepository.save(user);
    }

    @Override
    public void initDB() {

        if (userRepository.findAll().size() == 0) {

            //créa user
            User user1 = new User();
            user1.setUsername("john");
            user1.setPassword("john");
            user1.setCity("Bruxelles");
            user1.setProfilePicPath("chuck.jpg");
            user1.setCountry("Belgique");
            user1.setEmail("kdjd@ovh.be");
            user1.setDescription("<p>Cras malesuada, sem at finibus hendrerit, lorem nulla auctor tellus, vel cursus ligula lorem et nulla. Aenean molestie sem id lorem tincidunt vulputate. In eu interdum felis, vel mattis sapien. Quisque cursus velit non sem condimentum, eu viverra nibh aliquam. Morbi sed fringilla diam. Phasellus sed facilisis mauris. Morbi malesuada nunc sed eros rhoncus interdum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut accumsan ligula quis viverra sagittis. Nam in diam eget enim commodo vestibulum quis nec mauris. Aenean sit amet mauris eget risus convallis gravida at nec ante. Donec eu ligula magna. Aliquam et nisi a dui dignissim elementum.</p>\n" +
                    "\n" +
                    "<p>Etiam pretium mauris quam, gravida vehicula lacus gravida sed. Phasellus vulputate suscipit eleifend. Donec quis turpis tincidunt, mollis ex vitae, dapibus enim. Nam laoreet turpis a est consectetur, quis aliquet tortor molestie. Maecenas tincidunt est non maximus dictum. Sed quis tellus ac nisi vehicula vehicula. Curabitur rhoncus turpis non velit sollicitudin, dictum pretium nisi viverra.</p>");

            Authority role_user = this.createOrGetAuthority("ROLE_USER");
            user1.getAuthorities().add(role_user);

            User user2 = new User();
            user2.setUsername("paul");
            user2.setPassword("paul");
            user2.setProfilePicPath("george.jpg");
            user2.setCity("London");
            user2.setCountry("Angleterre");
            user2.setEmail("paul@ovh.be");
            user2.setDescription("<p>Cras malesuada, sem at finibus hendrerit, lorem nulla auctor tellus, vel cursus ligula lorem et nulla. Aenean molestie sem id lorem tincidunt vulputate. In eu interdum felis, vel mattis sapien. Quisque cursus velit non sem condimentum, eu viverra nibh aliquam. Morbi sed fringilla diam. Phasellus sed facilisis mauris. Morbi malesuada nunc sed eros rhoncus interdum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut accumsan ligula quis viverra sagittis. Nam in diam eget enim commodo vestibulum quis nec mauris. Aenean sit amet mauris eget risus convallis gravida at nec ante. Donec eu ligula magna. Aliquam et nisi a dui dignissim elementum.</p>\n" +
                    "\n" +
                    "<p>Etiam pretium mauris quam, gravida vehicula lacus gravida sed. Phasellus vulputate suscipit eleifend. Donec quis turpis tincidunt, mollis ex vitae, dapibus enim. Nam laoreet turpis a est consectetur, quis aliquet tortor molestie. Maecenas tincidunt est non maximus dictum. Sed quis tellus ac nisi vehicula vehicula. Curabitur rhoncus turpis non velit sollicitudin, dictum pretium nisi viverra.</p>");

            user2.getAuthorities().add(role_user);

            User admin = new User();
            admin.setUsername("admin");
            admin.setPassword("admin");
            admin.setCity("admintown");
            admin.setProfilePicPath("illuminati.jpg");
            admin.setCountry("Bouglique");
            admin.setEmail("admin@ovh.be");
            admin.setDescription("<p>Cras malesuada, sem at finibus hendrerit, lorem nulla auctor tellus, vel cursus ligula lorem et nulla. Aenean molestie sem id lorem tincidunt vulputate. In eu interdum felis, vel mattis sapien. Quisque cursus velit non sem condimentum, eu viverra nibh aliquam. Morbi sed fringilla diam. Phasellus sed facilisis mauris. Morbi malesuada nunc sed eros rhoncus interdum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut accumsan ligula quis viverra sagittis. Nam in diam eget enim commodo vestibulum quis nec mauris. Aenean sit amet mauris eget risus convallis gravida at nec ante. Donec eu ligula magna. Aliquam et nisi a dui dignissim elementum.</p>\n" +
                    "\n" +
                    "<p>Etiam pretium mauris quam, gravida vehicula lacus gravida sed. Phasellus vulputate suscipit eleifend. Donec quis turpis tincidunt, mollis ex vitae, dapibus enim. Nam laoreet turpis a est consectetur, quis aliquet tortor molestie. Maecenas tincidunt est non maximus dictum. Sed quis tellus ac nisi vehicula vehicula. Curabitur rhoncus turpis non velit sollicitudin, dictum pretium nisi viverra.</p>");

            Authority role_admin = this.createOrGetAuthority("ROLE_ADMIN");
            admin.getAuthorities().add(role_user);
            admin.getAuthorities().add(role_admin);

            this.signUp(user1);
            this.signUp(user2);
            this.signUp(admin);

            //créa catégories
            Category c1 = new Category();
            c1.setName("Post-bop");
            Category c2 = new Category();
            c2.setName("Jazz modal");
            Category c3 = new Category();
            c3.setName("Jazz fusion");
            Category c4 = new Category();
            c4.setName("Autre");

            categoryRepository.save(c1);
            categoryRepository.save(c2);
            categoryRepository.save(c3);
            categoryRepository.save(c4);

            //créa review
            Review r = new Review();
            r.setAuthor(user1);
            r.setCategory(c4);
            r.setTitle("title test review");
            r.setContent("<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis velit in lacus posuere, quis egestas odio eleifend. Aliquam eget tellus commodo, consequat enim non, auctor mauris. Pellentesque dictum sodales enim, eget venenatis arcu auctor tincidunt. Suspendisse ut volutpat ligula. Aenean dignissim tincidunt eros dictum suscipit. Integer nec aliquam odio. Mauris a aliquam lacus, non pulvinar nisl. Donec suscipit nibh quis libero vestibulum elementum. Sed eget dolor iaculis, rhoncus dolor nec, faucibus ex. Nam sit amet tincidunt dui. Proin efficitur neque est, in mattis orci porta nec. Nulla imperdiet efficitur mi. Vestibulum ac velit sit amet dolor luctus laoreet. Sed eget interdum eros, sed imperdiet lacus.\n"
                    + "\n"
                    + "Maecenas non elit</p><p> nec eros volutpat sagittis id vitae sem. Mauris semper consequat euismod. Sed vel ullamcorper ex. Nullam sit amet eleifend nisl. Pellentesque fringilla rhoncus eros, quis bibendum elit lobortis ut. Suspendisse ac accumsan tortor. Integer aliquet nunc mauris, ornare semper arcu iaculis vitae. Donec eu ante elit. Etiam non nunc dui.</p>");
            r.setCreationDate(new Date());
            r.setPicturePath("terminator.jpg");
            reviewRepository.save(r);

            Review r2 = new Review();
            r2.setAuthor(user1);
            r2.setCategory(c1);
            r2.setTitle("Ma review 2");
            r2.setContent("<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis velit in lacus posuere, quis egestas odio eleifend. Aliquam eget tellus commodo, consequat enim non, auctor mauris. Pellentesque dictum sodales enim, eget venenatis arcu auctor tincidunt. Suspendisse ut volutpat ligula. Aenean dignissim tincidunt eros dictum suscipit. Integer nec aliquam odio. Mauris a aliquam lacus, non pulvinar nisl. Donec suscipit nibh quis libero vestibulum elementum. Sed eget dolor iaculis, rhoncus dolor nec, faucibus ex. Nam sit amet tincidunt dui. Proin efficitur neque est, in mattis orci porta nec. Nulla imperdiet efficitur mi. Vestibulum ac velit sit amet dolor luctus laoreet. Sed eget interdum eros, sed imperdiet lacus.\n"
                    + "\n"
                    + "Maecenas non elit</p><p> nec eros volutpat sagittis id vitae sem. Mauris semper consequat euismod. Sed vel ullamcorper ex. Nullam sit amet eleifend nisl. Pellentesque fringilla rhoncus eros, quis bibendum elit lobortis ut. Suspendisse ac accumsan tortor. Integer aliquet nunc mauris, ornare semper arcu iaculis vitae. Donec eu ante elit. Etiam non nunc dui.</p>");
            r2.setCreationDate(new Date());

            reviewRepository.save(r2);

            Review r3 = new Review();
            r3.setAuthor(user1);
            r3.setCategory(c4);
            r3.setTitle("Autre review 3");
            r3.setContent("<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis velit in lacus posuere, quis egestas odio eleifend. Aliquam eget tellus commodo, consequat enim non, auctor mauris. Pellentesque dictum sodales enim, eget venenatis arcu auctor tincidunt. Suspendisse ut volutpat ligula. Aenean dignissim tincidunt eros dictum suscipit. Integer nec aliquam odio. Mauris a aliquam lacus, non pulvinar nisl. Donec suscipit nibh quis libero vestibulum elementum. Sed eget dolor iaculis, rhoncus dolor nec, faucibus ex. Nam sit amet tincidunt dui. Proin efficitur neque est, in mattis orci porta nec. Nulla imperdiet efficitur mi. Vestibulum ac velit sit amet dolor luctus laoreet. Sed eget interdum eros, sed imperdiet lacus.\n"
                    + "\n"
                    + "Maecenas non elit</p><p> nec eros volutpat sagittis id vitae sem. Mauris semper consequat euismod. Sed vel ullamcorper ex. Nullam sit amet eleifend nisl. Pellentesque fringilla rhoncus eros, quis bibendum elit lobortis ut. Suspendisse ac accumsan tortor. Integer aliquet nunc mauris, ornare semper arcu iaculis vitae. Donec eu ante elit. Etiam non nunc dui.</p>");
            r3.setCreationDate(new Date());
            r3.setPicturePath("8cb25417d5b611e2f85abd0bf4b3d08d.jpg");
            reviewRepository.save(r3);

            Review r4 = new Review();
            r4.setAuthor(user1);
            r4.setCategory(c4);
            r4.setTitle("Miles Davis, Sketches");
            r4.setPicturePath("5431f1df49da16bf1eff22beed2ec391.jpg");
            r4.setContent("<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis velit in lacus posuere, quis egestas odio eleifend. Aliquam eget tellus commodo, consequat enim non, auctor mauris. Pellentesque dictum sodales enim, eget venenatis arcu auctor tincidunt. Suspendisse ut volutpat ligula. Aenean dignissim tincidunt eros dictum suscipit. Integer nec aliquam odio. Mauris a aliquam lacus, non pulvinar nisl. Donec suscipit nibh quis libero vestibulum elementum. Sed eget dolor iaculis, rhoncus dolor nec, faucibus ex. Nam sit amet tincidunt dui. Proin efficitur neque est, in mattis orci porta nec. Nulla imperdiet efficitur mi. Vestibulum ac velit sit amet dolor luctus laoreet. Sed eget interdum eros, sed imperdiet lacus.\n"
                    + "\n"
                    + "Maecenas non elit</p><p> nec eros volutpat sagittis id vitae sem. Mauris semper consequat euismod. Sed vel ullamcorper ex. Nullam sit amet eleifend nisl. Pellentesque fringilla rhoncus eros, quis bibendum elit lobortis ut. Suspendisse ac accumsan tortor. Integer aliquet nunc mauris, ornare semper arcu iaculis vitae. Donec eu ante elit. Etiam non nunc dui.</p>");
            r4.setCreationDate(new Date());
            reviewRepository.save(r4);

            Review r5 = new Review();
            r5.setAuthor(admin);
            r5.setCategory(c2);
            r5.setTitle("Robert downey, jazz expert");
            r5.setPicturePath("Jazz.jpg");
            r5.setContent("<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis velit in lacus posuere, quis egestas odio eleifend. Aliquam eget tellus commodo, consequat enim non, auctor mauris. Pellentesque dictum sodales enim, eget venenatis arcu auctor tincidunt. Suspendisse ut volutpat ligula. Aenean dignissim tincidunt eros dictum suscipit. Integer nec aliquam odio. Mauris a aliquam lacus, non pulvinar nisl. Donec suscipit nibh quis libero vestibulum elementum. Sed eget dolor iaculis, rhoncus dolor nec, faucibus ex. Nam sit amet tincidunt dui. Proin efficitur neque est, in mattis orci porta nec. Nulla imperdiet efficitur mi. Vestibulum ac velit sit amet dolor luctus laoreet. Sed eget interdum eros, sed imperdiet lacus.\n"
                    + "\n"
                    + "Maecenas non elit</p><p> nec eros volutpat sagittis id vitae sem. Mauris semper consequat euismod. Sed vel ullamcorper ex. Nullam sit amet eleifend nisl. Pellentesque fringilla rhoncus eros, quis bibendum elit lobortis ut. Suspendisse ac accumsan tortor. Integer aliquet nunc mauris, ornare semper arcu iaculis vitae. Donec eu ante elit. Etiam non nunc dui.</p>");
            r5.setCreationDate(new Date());

            reviewRepository.save(r5);

            Review r6 = new Review();
            r6.setAuthor(user2);
            r6.setCategory(c3);
            r6.setTitle("Jazz kung fu");
            r6.setContent("<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis velit in lacus posuere, quis egestas odio eleifend. Aliquam eget tellus commodo, consequat enim non, auctor mauris. Pellentesque dictum sodales enim, eget venenatis arcu auctor tincidunt. Suspendisse ut volutpat ligula. Aenean dignissim tincidunt eros dictum suscipit. Integer nec aliquam odio. Mauris a aliquam lacus, non pulvinar nisl. Donec suscipit nibh quis libero vestibulum elementum. Sed eget dolor iaculis, rhoncus dolor nec, faucibus ex. Nam sit amet tincidunt dui. Proin efficitur neque est, in mattis orci porta nec. Nulla imperdiet efficitur mi. Vestibulum ac velit sit amet dolor luctus laoreet. Sed eget interdum eros, sed imperdiet lacus.\n"
                    + "\n"
                    + "Maecenas non elit</p><p> nec eros volutpat sagittis id vitae sem. Mauris semper consequat euismod. Sed vel ullamcorper ex. Nullam sit amet eleifend nisl. Pellentesque fringilla rhoncus eros, quis bibendum elit lobortis ut. Suspendisse ac accumsan tortor. Integer aliquet nunc mauris, ornare semper arcu iaculis vitae. Donec eu ante elit. Etiam non nunc dui.</p>");
            r6.setCreationDate(new Date());
            r6.setPicturePath("Screen-Shot-2015-08-29-at-2-53-28-PM.jpg");
            reviewRepository.save(r6);

            //créa comm
            Comm com = new Comm();
            com.setAuthor(user1);
            com.setContent("je suis content");
            com.setCreationDate(new Date());
            com.setReview(r);
            commentRepository.save(com);

            //créa circle avec membres
            Circle circle = new Circle();
            circle.setCircleAdmin(user1);
            circle.setCircleName("init circle");
            circle.setDescription("cercle de test gros");
            circle.getMembers().add(user1);
            circle.getMembers().add(user2);
            circle.getMembers().add(admin);
            circleRepository.save(circle);



        }
    }

    @Override
    public void updateUser(User u) {
        userRepository.save(u);
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public int computeLikes(User user) {

        int total = 0;
        for(Review r : reviewRepository.findByAuthor(user)) {
            total += r.getLikes();
        }
        return total;
    }

    @Override
    public List<User> findByUsernameLike(String name_startsWith) {
        return userRepository.findByUsernameLike(name_startsWith);
    }


}
