package com.jazzreviews.service;


import com.jazzreviews.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();

    Category findByName(String category);

    Category createOrGetCategory(String category);

}
