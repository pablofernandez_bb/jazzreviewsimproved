
package com.jazzreviews.entity;

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "user")
public class User implements Serializable, UserDetails {
    
    @Id
    @GeneratedValue
    private Long id;

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String country;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfBirth;
    @Column(length = 20000)
    private String description;
    private String profilePicPath;
    private boolean messageUnread;
    private boolean accountNonLocked;
    private boolean accountNonExpired;
    private boolean credentialsNonExpired;
    private boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Authority> authorities;
    
    @OneToMany(mappedBy = "author", fetch = FetchType.EAGER)
    private Set<Review> reviews;
    
    @OneToMany(mappedBy = "author",fetch = FetchType.EAGER)
    private Set<Comm> comments;

    @OneToMany(mappedBy = "destinationUser",fetch = FetchType.EAGER)
    private Set<PrivateMessage> privateMessages;

    @ManyToMany(mappedBy = "members", fetch = FetchType.EAGER)
    private Set<Circle> belongingCircles;

    @ManyToMany(mappedBy = "likingUsers",fetch = FetchType.EAGER)
    private Set<Review> likedReviews;




    /******
     *
     * GETTER/SETTER
     *
     ******/

    public boolean hasMessageUnread() {
        return messageUnread;
    }

    public void setMessageUnread(boolean messageUnread) {
        this.messageUnread = messageUnread;
    }

    public Set<Review> getLikedReviews() {
        if (likedReviews == null) {
            likedReviews = new HashSet<>();
        }
        return likedReviews;
    }

    public void setLikedReviews(Set<Review> likedReviews) {
        this.likedReviews = likedReviews;
    }

    public Set<Circle> getBelongingCircles() {
        if (belongingCircles == null) {
            belongingCircles = new HashSet<>();
        }
        return belongingCircles;
    }

    public void setBelongingCircles(Set<Circle> belongingCircles) {
        this.belongingCircles = belongingCircles;
    }

    public Set<PrivateMessage> getPrivateMessages() {
        if (privateMessages == null) {
            privateMessages = new HashSet<>();
        }
        return privateMessages;
    }

    public void setPrivateMessages(Set<PrivateMessage> privateMessages) {
        this.privateMessages = privateMessages;
    }

    public Set<Comm> getComments() {
        if (comments == null) {
            comments = new HashSet<>();
        }
        
        return comments;
    }

    public Set<Review> getReviews() {
        if (reviews == null) {
            reviews = new HashSet<>();
        }
        return reviews;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public void setComments(Set<Comm> comments) {
        this.comments = comments;
    }

    public String getProfilePicPath() {
        return profilePicPath;
    }

    public void setProfilePicPath(String profilePicPath) {
        this.profilePicPath = profilePicPath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }      

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public Set<Authority> getAuthorities() {
        if (authorities == null) {
            authorities = new HashSet<>();
        }

        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String toString() {
        return username ;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (getId() != null ? !getId().equals(user.getId()) : user.getId() != null) return false;
        return getUsername() != null ? getUsername().equals(user.getUsername()) : user.getUsername() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getUsername() != null ? getUsername().hashCode() : 0);
        return result;
    }
}
