package com.jazzreviews.repository;

import com.jazzreviews.entity.SubjectAnswer;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SubjectAnswerRepository extends JpaRepository<SubjectAnswer,Long> {
}
