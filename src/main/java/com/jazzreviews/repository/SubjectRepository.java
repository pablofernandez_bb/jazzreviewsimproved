package com.jazzreviews.repository;

import com.jazzreviews.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SubjectRepository extends JpaRepository<Subject,Long> {

    Subject findById(Long id);
}
