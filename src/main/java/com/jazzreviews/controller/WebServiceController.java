package com.jazzreviews.controller;


import com.jazzreviews.entity.Review;
import com.jazzreviews.entity.User;
import com.jazzreviews.service.ReviewService;
import com.jazzreviews.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class WebServiceController {

    @Autowired
    UserService userService;

    @Autowired
    ReviewService reviewService;


    @RequestMapping("/getusernames")
    public List<String> getUsernames(@RequestParam String name_startsWith, @RequestParam int maxRows) {

        List<User> allUsers = userService.findAll();
        List<String> usernames = new ArrayList<>();

        for(User u : allUsers) {
            usernames.add(u.getUsername());
        }

        usernames.removeIf(u -> !u.contains(name_startsWith));

        return usernames;
    }

    @RequestMapping("/getusernames2")
    public List<String> getUsernames2() {

        List<User> allUsers = userService.findAll();
        List<String> usernames = new ArrayList<>();

        for(User u : allUsers) {
            usernames.add(u.getUsername());
        }

        return usernames;
    }

    @RequestMapping("/updatelike")
    public boolean updateLike(@RequestParam Long reviewid, @AuthenticationPrincipal User user) {

        Review foundReview = reviewService.findById(reviewid);
        boolean addLike;

        //le user aime la review et incrémente le compteur
        if (! foundReview.getLikingUsers().contains(user)) {

            foundReview.setLikes(foundReview.getLikes() + 1);

            foundReview.getLikingUsers().add(user);

            addLike = true;

        // le user l'aime déjà, il supprime son like
        } else {
            foundReview.setLikes(foundReview.getLikes() - 1);

            foundReview.getLikingUsers().remove(user);

            addLike = false;
        }


        reviewService.saveReview(foundReview);
        userService.updateUser(user);

        return addLike;
    }

    @RequestMapping("/doesitlikeit")
    public boolean doesItLikeIt(@AuthenticationPrincipal User user, @RequestParam Long reviewid) {

        return reviewService.findById(reviewid).getLikingUsers().contains(user);

    }

    @RequestMapping("/checkmessages")
    public boolean checkMessages(@RequestParam(defaultValue = "null") String username) {

        if (username == "null")
            return false;

        User foundUser = userService.findByUsername(username);

        if (foundUser == null)
            return false;

        return foundUser.hasMessageUnread();
    }

    /*
    @ExceptionHandler(Exception.class)
    public String handleGeneralException(HttpServletRequest req, Exception e, Model model) throws Exception {

        model.addAttribute("exception",e);
        model.addAttribute("url",req.getRequestURL());

        return "publicview/errorpage";
    }
    */

}
