
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>admin console</title>
    <c:import url="../includes/bootstrap.jsp" />
</head>
<body>
<c:import url="../includes/navbar.jsp" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="text-center">admin console</h1>
        <div class="container">
            <a href="<c:url value="/admin/supercreate"/>"><button  type="button" class="btn btn-default">create user</button></a>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>username</th>
                    <th>Email</th>
                    <th>Enabled</th>
                    <th>Roles</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${usersList}" var="user" >
                    <tr>
                        <td>${user.id}</td>
                        <td><a href="<c:url value="/profilepage/${user.username}" />">${user.username}</a></td>
                        <td>${user.email}</td>
                        <td><a href="<c:url value="/admin/disableuser/${user.id}" />">${user.enabled}</a></td>
                        <td><c:forEach items="${user.authorities}" var="authority">${authority.authority} - </c:forEach> </td>
                        <td><a href="<c:url value="/admin/editmember/${user.id}"/> ">edit</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>


</div>

</body>
</html>