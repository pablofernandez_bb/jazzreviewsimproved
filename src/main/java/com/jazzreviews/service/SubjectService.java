package com.jazzreviews.service;


import com.jazzreviews.entity.Subject;

public interface SubjectService {

    void saveSubject(Subject s);

    Subject findById(Long subjectid);
}
