package com.jazzreviews.service;


import com.jazzreviews.entity.Circle;

import java.util.List;


public interface CircleService {

    Circle saveCircle(Circle g);

    Circle findById(Long id);

    void deleteCircle(Circle foundCircle);

    List<Circle> findAll();
}
