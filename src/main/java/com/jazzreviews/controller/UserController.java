
package com.jazzreviews.controller;

import com.jazzreviews.entity.Category;
import com.jazzreviews.entity.Comm;
import com.jazzreviews.entity.Review;
import com.jazzreviews.entity.User;
import com.jazzreviews.model.FileModel;
import com.jazzreviews.service.CategoryService;
import com.jazzreviews.service.CommentService;
import com.jazzreviews.service.ReviewService;
import com.jazzreviews.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    
    @Autowired
    ReviewService reviewService;    
    
    @Autowired
    UserService userService;
    
    @Autowired
    CommentService commentService;

    @Autowired
    CategoryService categoryService;
    
    @RequestMapping("/dashboard")
    public String dashboard(Model model, @AuthenticationPrincipal User user){
        
        model.addAttribute("user",userService.findById(user.getId()));
        model.addAttribute("totalLikes",userService.computeLikes(user));

        
        return "userview/dashboard";
    }
    
    @RequestMapping("/deletereview/{id}")
    public String deleteReview(@PathVariable Long id, @AuthenticationPrincipal User user) {
        
        Review futureDead = reviewService.findById(id);

        if (! futureDead.getAuthor().getUsername().equals(user.getUsername())) {
            return "/publicview/forbiddenpage";
        }

        for(Comm c : futureDead.getComments()) {
            commentService.deleteComment(c.getId());
        }

        reviewService.deleteReview(futureDead.getId());
        
        return "userview/dashboard";
    }
    
    @RequestMapping("/myreviews")
    public String myReviews(@AuthenticationPrincipal User user, Model model) {
        
        User foundUser = userService.findByUsername(user.getUsername());
        List<Review> myReviews = reviewService.findByAuthor(foundUser);
        model.addAttribute("myReviews", myReviews);
        
        return "userview/myreviews";
    }

    
    @RequestMapping(value = "/writereview", method = RequestMethod.GET)
    public String writeReview(Model model){


        model.addAttribute("categories",categoryService.findAll());
        return "userview/writereview";
    }
    
    @RequestMapping(value = "/writereview", method = RequestMethod.POST)
    public String writeReviewProcess(@RequestParam String review, @AuthenticationPrincipal User user, @RequestParam String title, @RequestParam String category){

        Review myReview = new Review();
        User author = userService.findByUsername(user.getUsername());

        Category currentCategory = categoryService.createOrGetCategory(category);

        myReview.setCategory(currentCategory);
        myReview.setAuthor(author);
        myReview.setTitle(title);
        myReview.setContent(review);
        myReview.setCreationDate(new Date());        

        Review savedReview = reviewService.saveReview(myReview);

        if (savedReview == null) {
            return "publicview/errorpage";
        }
        
        return "redirect:/user/uploadreviewpic/" + savedReview.getId();
    }
    
    @RequestMapping(value = "/editreview/{id}", method = RequestMethod.GET)
    public String editReviewPage(@AuthenticationPrincipal User user, @PathVariable Long id, Model model, FileModel fileModel) {

        Review currentReview = reviewService.findById(id);

        if (! currentReview.getAuthor().getUsername().equals(user.getUsername())) {
            return "/publicview/forbiddenpage";
        }

        model.addAttribute("fileModel",fileModel);
        model.addAttribute("foundReview",reviewService.findById(id));
        
        return "userview/editreview";
    }
    
    @RequestMapping(value="/editreview", method = RequestMethod.POST)
    public String editReview(@AuthenticationPrincipal User user, @RequestParam Long id, @RequestParam String review, @RequestParam String title) {
        
        Review reviewToEdit = reviewService.findById(id);

        if (! reviewToEdit.getAuthor().getUsername().equals(user.getUsername())) {
            return "/publicview/forbiddenpage";
        }

        reviewToEdit.setTitle(title);
        reviewToEdit.setContent(review);
        
        reviewService.saveReview(reviewToEdit);
        
        return "redirect:/user/dashboard";
        
    }

    @RequestMapping("/writecomment")
    public String writeComment(@RequestParam String comment, @AuthenticationPrincipal User user, @RequestParam String reviewid) {
        
        User foundUser = userService.findByUsername(user.getUsername());
        Review review = reviewService.findById(Long.parseLong(reviewid));
        
        Comm commentToSave = new Comm();
        commentToSave.setAuthor(foundUser);
        commentToSave.setContent(comment);
        commentToSave.setCreationDate(new Date());
        commentToSave.setReview(review);
        
        commentService.saveComment(commentToSave);
        
        return "redirect:/reviewpage/" + reviewid;
    }

    @RequestMapping("/editprofile")
    public String editProfilePage(@AuthenticationPrincipal User user, Model model) {

        User foundUser = userService.findByUsername(user.getUsername());
        model.addAttribute("user",foundUser);
        
        FileModel fileModel = new FileModel();
        model.addAttribute("fileModel", fileModel);
        return "userview/editprofile";
    }
    
    @RequestMapping("/editdescription")
    public String editProfile(@AuthenticationPrincipal User user, @RequestParam String description) {
        User u = userService.findByUsername(user.getUsername());
        u.setDescription(description);
        
        userService.updateUser(u);
        
        return "redirect:/user/dashboard";
                
        
    }

    @RequestMapping("/uploadprofilepic")
    public String uploadProfilePic(FileModel fileModel, @AuthenticationPrincipal User user) throws IOException {

        MultipartFile multipartFile = fileModel.getFile();
        String hashedName = user.getUsername() + Math.random() * 100 + fileModel.getFile().getOriginalFilename();
        User userToUpdate = userService.findByUsername(user.getUsername());

        if (userToUpdate.getProfilePicPath() != null) {
            File profilePicToKill = new File("C:/tmp/profilepic/" + userToUpdate.getProfilePicPath());
            profilePicToKill.createNewFile();
            profilePicToKill.delete();
        }
        FileCopyUtils.copy(multipartFile.getBytes(), new File("C:/tmp/profilepic/" + hashedName));

        userToUpdate.setProfilePicPath(hashedName);
        userService.updateUser(userToUpdate);

        return "redirect:/user/editprofile";
    }

    @RequestMapping(value = "/uploadreviewpic/{reviewid}", method = RequestMethod.GET)
    public String uploadReviewPic(@PathVariable Long reviewid, Model model) {
        FileModel fileModel = new FileModel();
        model.addAttribute("fileModel", fileModel);
        model.addAttribute("reviewid",reviewid);

        return "userview/uploadreviewpic";
    }

    @RequestMapping(value = "/upload/reviewpic", method = RequestMethod.POST)
    public String uploadReviewPic2(@RequestParam Long reviewid, FileModel fileModel) throws IOException {

        MultipartFile multipartFile = fileModel.getFile();
        String hashedName = Math.random() * 100 + fileModel.getFile().getOriginalFilename();

        FileCopyUtils.copy(multipartFile.getBytes(), new File("C:/tmp/reviewpic/" + hashedName));

        Review reviewToUpdate = reviewService.findById(reviewid);

        if (reviewToUpdate.getPicturePath() != null) {
            File reviewPicToKill = new File("C:/tmp/jazzreviews/reviewpic/" + reviewToUpdate.getPicturePath());
            reviewPicToKill.createNewFile();
            reviewPicToKill.delete();
        }

        reviewToUpdate.setPicturePath(hashedName);
        reviewService.saveReview(reviewToUpdate);


        return "redirect:/user/dashboard";
    }

    @RequestMapping("/likereview/{reviewid}")
    public String likeReview(@PathVariable Long reviewid, @AuthenticationPrincipal User user) {

        Review foundReview = reviewService.findById(reviewid);

        if (foundReview.getLikingUsers().contains(user)) {
            return "redirect:/reviewpage/" + reviewid;
        }

        foundReview.setLikes(foundReview.getLikes() + 1);

        foundReview.getLikingUsers().add(user);

        reviewService.saveReview(foundReview);
        userService.updateUser(user);

        return "redirect:/reviewpage/" + reviewid;
    }

    @ExceptionHandler(Exception.class)
    public String handleGeneralException(HttpServletRequest req, Exception e, Model model) throws Exception {

        model.addAttribute("exception",e);
        model.addAttribute("url",req.getRequestURL());

        return "publicview/errorpage";
    }
    
}
