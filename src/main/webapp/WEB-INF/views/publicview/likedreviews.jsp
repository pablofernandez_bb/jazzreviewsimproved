<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>reviews likée par ${user.username}</title>
    <c:import url="../includes/bootstrap.jsp"/>
</head>
<body>
<c:import url="../includes/navbar.jsp"/>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="text-center">reviews likée par ${user.username}</h1>
        <br/>
        <c:if test="${user.likedReviews.size() == 0}">
            <p class="text-center">aucune pour l'instant :( </p>
        </c:if>
        <c:forEach items="${user.likedReviews}" var="review">

            <div class="blocReview well col-md-3 col-md-offset-1">
                <a href="<c:url value="/reviewpage/${review.id}"/>">
                    <div class="img-mini img-thumbnail"
                         style="background-image: url('<c:url value="/download/reviewpic/${review.id}"/>');"></div>
                    <c:set var="fullTitle" value="${review.title}"/>
                    <c:set var="shortTitle" value="${fn:substring(fullTitle, 0, 18)}"/>
                    <h3 class="text-center">${shortTitle}</h3>
                    <p>catégorie : <strong><a
                            href="<c:url value="/showcategory/${review.category.name}"/>">${review.category.name}</a></strong>
                    </p>
                    <p>auteur : <strong><a
                            href="<c:url value="/profilepage/${review.author}"/>">${review.author}</a></strong></p>
                    <p class="text-center"><span class="glyphicon glyphicon-heart"></span> ${review.likes}</p>
                </a>
            </div>

        </c:forEach>
    </div>


</div>

</body>
</html>