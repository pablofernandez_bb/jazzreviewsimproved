
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>message de ${privateMessage.author}</title>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
    <c:import url="../includes/bootstrap.jsp" />
</head>
<body>
<c:import url="../includes/navbar.jsp" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1>message privé</h1>
        <a href="<c:url value="/user/privatemessage/inbox"/>"><button class="btn btn-default"> retour inbox </button></a>
        <br />
        <br />
        <div class="well">
            <p><strong><a href="<c:url value="/profilepage/${privateMessage.author}" />" >${privateMessage.author}</a></strong></p>
            <p>${privateMessage.sendingTime}</p>
            <p>${privateMessage.content}</p>
        </div>
        <div class="well">
            <h3>répondre :</h3>
            <form role="form" method="post" action="<c:url value="/user/privatemessage/new" />" >

                <div class="form-group">
                    <label for="destination">destinataire</label>
                    <input class="form-control" id="destination" type="hidden" name="destinationUser" value="${privateMessage.author}" required />
                </div>
                <div class="form-group">
                    <label for="messageBody">Votre message:</label>
                    <textarea class="form-control" rows="8" id="messageBody" name="messageBody"></textarea>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>


</div>

</body>
</html>