
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>éditer votre profil</title>
        <c:import url="../includes/bootstrap.jsp" />
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
    </head>
    <c:import url="../includes/navbar.jsp" />
    <body>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1>éditer votre profil</h1>
                <sec:authentication property="principal.username" var="username"></sec:authentication>
                <img width="200" src="<c:url value="/download/profilepic/${username}"/>" />


                <form:form method="post" action="uploadprofilepic" enctype="multipart/form-data" modelAttribute="fileModel">
                    <div class="form-group" >
                        <form:input path="file" type="file" class="form-control" accept="image/*" />
                        <input type="submit" value="upload" class="btn btn-primary" />
                    </div>
                </form:form>

                <form method="post" action="<c:url value="/user/editdescription" /> ">
                    <div class="form-group">
                        <label for="comment">Votre présentation:</label>
                        <textarea class="form-control" rows="8" id="comment" name="description">${user.description}</textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>


            </div>
        </div>
    </body>
</html>
