
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>retirer un membre</title>
    <c:import url="../includes/bootstrap.jsp" />
</head>
<body>
<c:import url="../includes/navbar.jsp" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1>retirer un membre</h1>
        <p>cliquer pour retirer</p>
        <ul>
            <c:forEach items="${circle.members}" var="member">
                <li><a href="<c:url value="/user/circle/removemember/${member.id}/${circle.id}"/>">${member.username}</a></li>
            </c:forEach>
        </ul>
    </div>


</div>

</body>
</html>