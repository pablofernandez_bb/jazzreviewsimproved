
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>toutes les reviews</title>
        <c:import url="../includes/bootstrap.jsp" />
    </head>
    <body>
        <c:import url="../includes/navbar.jsp" />
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1 class="text-center">toutes les reviews</h1>


                <c:forEach var="reviewList" items="${reviewList}">
                    <div class="well">
                        <a href="<c:url value="/reviewpage/${reviewList.id}" />">${reviewList.title} </a><em>(écrit le <fmt:formatDate value="${reviewList.creationDate}" pattern="dd-MM-yyyy" />)</em> Auteur :
                        <strong><a href="<c:url value="/profilepage/${reviewList.author}" />"> ${reviewList.author}</a> </strong>
                    </div>
                </c:forEach>
            </div>
        </div>
    </body>
</html>
