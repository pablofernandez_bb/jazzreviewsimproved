package com.jazzreviews.service.impl;

import com.jazzreviews.entity.PrivateMessage;
import com.jazzreviews.repository.PrivateMessageRepository;
import com.jazzreviews.service.PrivateMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class PrivateMessageServiceImpl implements PrivateMessageService{

    @Autowired
    PrivateMessageRepository privateMessageRepository;



    @Override
    public PrivateMessage savePrivateMessage(PrivateMessage pm) {
        return privateMessageRepository.save(pm);
    }

    @Override
    public PrivateMessage findById(Long messageid) {
        return privateMessageRepository.findById(messageid);
    }
}
