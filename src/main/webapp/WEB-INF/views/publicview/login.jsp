
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>login</title>
        <c:import url="../includes/bootstrap.jsp" />
    </head>
    <body>
        <c:import url="../includes/navbar.jsp" />
        
        <div class='row'>
            <h1 class="text-center">s'identifier</h1>
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <form role="form" method="post" action="<c:url value="/check"/>"  >
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="text" class="form-control first-input" id="username" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control" id="pwd" name="password" required>
                    </div>
                    <div class="text-center"><button type="submit" class="btn btn-default text-center">Submit</button></div>
                </form>
                <br/>
                <br/>
                <c:if test="${msg == 'success'}" >
                    <div class="alert alert-success">vous êtes correctement inscrit, identifiez-vous à présent</div>
                </c:if>
                <c:if test="${msg == 'failed'}" >
                    <div class="alert alert-danger">problème lors de l'inscription</div>
                </c:if>

                <c:if test="${error}">
                    <div class="alert alert-danger text-center">votre mot de passe ou nom d'utilisateur sont erronés</div>
                </c:if>
            </div>
        </div>
        <script>
            $(function(){
                $(".first-input").focus();
            });
        </script>
    </body>
</html>
