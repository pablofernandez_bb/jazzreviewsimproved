package com.jazzreviews.service;


import com.jazzreviews.entity.PrivateMessage;

public interface PrivateMessageService {


    PrivateMessage savePrivateMessage(PrivateMessage pm);

    PrivateMessage findById(Long messageid);
}
