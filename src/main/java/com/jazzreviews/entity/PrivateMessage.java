package com.jazzreviews.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "privatemessage")
public class PrivateMessage implements Serializable, Comparable<PrivateMessage>{

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private User author;
    @ManyToOne
    private User destinationUser;
    @Column(length = 20000)
    private String content;
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendingTime;
    private String excerpt;

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public User getDestinationUser() {
        return destinationUser;
    }

    public void setDestinationUser(User destinationUser) {
        this.destinationUser = destinationUser;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getSendingTime() {
        return sendingTime;
    }

    public void setSendingTime(Date sendingTime) {
        this.sendingTime = sendingTime;
    }

    @Override
    public int compareTo(PrivateMessage o) {
        return o.getSendingTime().compareTo(this.getSendingTime());
    }
}
