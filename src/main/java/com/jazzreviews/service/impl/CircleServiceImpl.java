package com.jazzreviews.service.impl;


import com.jazzreviews.entity.Circle;
import com.jazzreviews.repository.CircleRepository;
import com.jazzreviews.service.CircleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class CircleServiceImpl implements CircleService {

    @Autowired
    CircleRepository circleRepository;

    @Override
    public Circle saveCircle(Circle g) {
        return circleRepository.save(g);
    }

    @Override
    public Circle findById(Long id) {
        return circleRepository.findById(id);
    }

    @Override
    public void deleteCircle(Circle foundCircle) {
        circleRepository.delete(foundCircle);
    }

    @Override
    public List<Circle> findAll() {
        return circleRepository.findAll();
    }
}
