package com.jazzreviews.repository;


import com.jazzreviews.entity.Circle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CircleRepository extends JpaRepository<Circle,Long>{

    Circle findById(Long id);
}
