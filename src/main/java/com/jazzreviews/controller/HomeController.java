
package com.jazzreviews.controller;

import com.jazzreviews.entity.Authority;
import com.jazzreviews.entity.Category;
import com.jazzreviews.entity.Review;
import com.jazzreviews.entity.User;
import com.jazzreviews.form.SignUpForm;
import com.jazzreviews.service.CategoryService;
import com.jazzreviews.service.ReviewService;
import com.jazzreviews.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/* TODO general list
*
*
*
*
*/



@Controller
@RequestMapping("")
public class HomeController {

    @Autowired
    UserService userService;
    
    @Autowired
    ReviewService reviewService;

    @Autowired
    CategoryService categoryService;


    @RequestMapping("")
    public String index(Model model) {
        userService.initDB();

        model.addAttribute("reviews",reviewService.findSixReviews());


        return "publicview/index";
    }
    
    @RequestMapping("index")
    public String toIndex() {
        return "publicview/index";
    }

    @RequestMapping("/search")
    public String search(@RequestParam String searchQuery, Model model) {
        
        List<Review> foundReviews = reviewService.seachReviews(searchQuery);
        
        model.addAttribute("foundReviews", foundReviews);
        
        return "publicview/searchresult";
    }

    @RequestMapping("/showcategory/{category}")
    public String showCategory(@PathVariable String category, Model model) {

        Category chosenCategory = categoryService.createOrGetCategory(category);

        model.addAttribute("reviews", reviewService.findByCategory(chosenCategory));
        model.addAttribute("category",category);

        return "publicview/reviewsbycategory";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signUp(Model model) {
        model.addAttribute("signUpForm", new SignUpForm());

        return "publicview/signup";
    }
    
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String processSignUp(@Valid SignUpForm signUpForm, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {

            return "publicview/signup";

        } else {

            User user = signUpForm.toUser();

            Authority role_user = userService.createOrGetAuthority("ROLE_USER");

            user.getAuthorities().add(role_user);

            User result = userService.signUp(user);

            String msg = "";
            if (result == null) {
                msg = "failed";
            } else {
                msg = "success";
            }

            model.addAttribute("msg",msg);

            return "publicview/login";
        }
    }

    @RequestMapping("/login")
    public String login(@RequestParam(defaultValue = "false") boolean error, Model model) {

        model.addAttribute("error",error);
        return "publicview/login";
    }

    @RequestMapping("/allreviews")
    public String allReviews(Model model) {

        List<Review> reviewList = reviewService.findAll();
        model.addAttribute("reviewList", reviewList);

        return "publicview/allreviews";
    }

    @RequestMapping("/reviewpage/{id}")
    public String reviewPage(@PathVariable Long id, Model model) {

        Review r = reviewService.findById(id);
        Set reviews = r.getAuthor().getReviews();

        model.addAttribute("review", r);

        return "publicview/reviewpage";
    }
    
    @RequestMapping("/reviewsbyauthor/{author}")
    public String reviewsByAuthor(@PathVariable String author, Model model) {
        
        User u = userService.findByUsername(author);
        ArrayList<Review> reviewList = (ArrayList<Review>) reviewService.findByAuthor(u);
        Collections.reverse(reviewList);
        model.addAttribute("foundReviews", reviewList);
        
        return "publicview/searchresult";
        
    }
    
    @RequestMapping("/profilepage/{username}")
    public String profilePage(@PathVariable String username, Model model) {
        
        User u = userService.findByUsername(username);        
        model.addAttribute("user",u);
        
        return "publicview/profilepage";
    }

    @RequestMapping(value = "/download/profilepic/{username}")
    public void downloadProfilePic(HttpServletResponse response, @PathVariable String username) throws IOException {

        User foundUser = userService.findByUsername(username);

        String filename = foundUser.getProfilePicPath();
        if (filename == null) {
            filename = "default.jpg";
        }

        File file = new File("C:/tmp/profilepic/" + filename);

        String mimeType = URLConnection.guessContentTypeFromName(filename);

        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }

        response.setHeader("Content-Disposition", "inline; filename=\"" + filename + "\"");
        response.setContentType(mimeType);
        response.setContentLength((int) file.length());

        InputStream stream = new BufferedInputStream(new FileInputStream(file));

        FileCopyUtils.copy(stream, response.getOutputStream());

    }

    @RequestMapping("/download/reviewpic/{reviewid}")
    public void downloadReviewPic(HttpServletResponse response, @PathVariable Long reviewid) throws IOException {

        Review foundReview = reviewService.findById(reviewid);

        String filename = foundReview.getPicturePath();
        if (filename == null) {
            filename = "default.jpg";
        }

        File file = new File("C:/tmp/reviewpic/" + filename);

        String mimeType = URLConnection.guessContentTypeFromName(filename);

        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }

        response.setHeader("Content-Disposition", "inline; filename=\"" + filename + "\"");
        response.setContentType(mimeType);
        response.setContentLength((int) file.length());

        InputStream stream = new BufferedInputStream(new FileInputStream(file));

        FileCopyUtils.copy(stream, response.getOutputStream());
    }

    @RequestMapping("/commentsbyauthor/{username}")
    public String commentsByAuthor(@PathVariable String username, Model model) {

        User foundUser = userService.findByUsername(username);
        model.addAttribute("nbComments",foundUser.getComments().size());
        model.addAttribute("user",foundUser);
        return "publicview/commentsbyauthor";
    }

    @RequestMapping("/topreviews")
    public String topReviews(Model model) {

        model.addAttribute("reviews",reviewService.findTop9ByOrderByLikesDesc());

        return "publicview/topreviews";

    }

    @RequestMapping("/likedreviews/{username}")
    public String likedReviews(@PathVariable String username, Model model) {

        User foundUser = userService.findByUsername(username);

        if (foundUser == null) {
            return "publicview/errorpage";
        }

        model.addAttribute("user",foundUser);

        return "publicview/likedreviews";

    }

    @RequestMapping("/categories")
    public String categories(Model model) {

        model.addAttribute("categories",categoryService.findAll());

        return "publicview/categories";

    }

    @ExceptionHandler(Exception.class)
    public String handleGeneralException(HttpServletRequest req, Exception e, Model model) throws Exception {

        model.addAttribute("exception",e);
        model.addAttribute("url",req.getRequestURL());

        return "publicview/errorpage";
    }

}
