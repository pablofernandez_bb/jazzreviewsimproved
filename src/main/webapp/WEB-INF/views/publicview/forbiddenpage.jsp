
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>forbidden</title>
    <c:import url="../includes/bootstrap.jsp" />
</head>
<body>
<c:import url="../includes/navbar.jsp" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8" >
        <h1 class="text-center">vous n'êtes pas autorisé à faire cela</h1>
        <br>
        <br>
        <div class="text-center"><img height="200px" src="<c:url value="/resources/img/forbidden.png"/>" /></div>
    </div>


</div>

</body>
</html>