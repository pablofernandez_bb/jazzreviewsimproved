package com.jazzreviews.service.impl;

import com.jazzreviews.entity.Category;
import com.jazzreviews.repository.CategoryRepository;
import com.jazzreviews.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findByName(String category) {
        return categoryRepository.findByName(category);
    }


    @Override
    public Category createOrGetCategory(String category) {
        Category result = categoryRepository.findByName(category);

        if (result == null) {
            Category newCategory = new Category();
            newCategory.setName(category);
            result = categoryRepository.save(newCategory);
        }

        return result;
    }

}
