<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>dashboard</title>
    <c:import url="../includes/bootstrap.jsp"/>
</head>
<body>
<c:import url="../includes/navbar.jsp"/>
<div class="row">

    <div class="col-md-offset-2 col-md-8">
        <div class="dashboardHeader"
             style="background-image: url('<c:url value="/resources/img/Trio_Balkan.jpg"/>');">
            hi
        </div>
        <h1 class="text-center" >Dashboard</h1>
        <br/>
        <div class="row">

            <p class="pull-right">total de likes sur mes reviews : ${totalLikes}</p>
        </div>
        <div class="row">
            <div class="col-md-offset-3 col-md-3" >
                <p><a href="<c:url value="/profilepage/${user.username}" /> ">
                    <button class="btn btn-primary btnDash">voir mon profil</button>
                </a></p>
                <p><a href="<c:url value="/user/editprofile" /> ">
                    <button class="btn btn-primary btnDash">éditer profil</button>
                </a></p>
                <p><a href="<c:url value="/user/circle/createcirclepage" /> ">
                    <button class="btn btn-primary btnDash">créer un cercle privé</button>
                </a></p>
            </div>
            <div class="col-md-3">
                <p><a href="<c:url value="/user/writereview" /> ">
                    <button class="btn btn-primary btnDash">écrire une review</button>
                </a></p>
                <p><a href="<c:url value="/user/myreviews" /> ">
                    <button class="btn btn-primary btnDash">éditer mes reviews</button>
                </a></p>
                <p><a href="<c:url value="/likedreviews/${user.username}" /> ">
                    <button class="btn btn-primary btnDash">reviews likées</button>
                </a></p>
            </div>
        </div>

        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <h3>pour admin</h3>
            <p><a href="<c:url value="/admin/seeallcircles" /> ">voir tous les cercles</a></p>
        </sec:authorize>
        <h3>cercles dont vous êtes membre</h3>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>nom du cercle</th>
                <th>admin</th>
                <th>nombre membres</th>
            </tr>
            </thead>
            <c:forEach items="${user.belongingCircles}" var="circle">

                <tr>
                    <td><strong><a
                            href="<c:url value="/user/circle/circlemainpage/${circle.id}" />">${circle.circleName}</a></strong>
                    </td>
                    <td><strong><a
                            href="<c:url value="/profilepage/${circle.circleAdmin.username}" />"> ${circle.circleAdmin.username}</a></strong>
                    </td>
                    <td>${circle.members.size()}</td>
                </tr>


            </c:forEach>
        </table>

        <h3>mes dernières reviews</h3>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>titre</th>
                <th>date</th>
                <th>nombre de likes</th>
            </tr>
            </thead>
            <c:forEach items="${user.reviews}" var="review">

                <tr>
                    <td><strong><a
                            href="<c:url value="/reviewpage/${review.id}" />">${review.title}</a></strong>
                    </td>
                    <td>
                        <fmt:formatDate value="${review.creationDate}" pattern="dd-MM-yyyy"/>
                    </td>
                    <td>${review.likes}</td>
                </tr>


            </c:forEach>
        </table>
    </div>
</div>


</body>
</html>
