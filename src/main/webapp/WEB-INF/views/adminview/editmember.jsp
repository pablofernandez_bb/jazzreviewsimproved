
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>modifier un membre</title>
    <c:import url="../includes/bootstrap.jsp" />
</head>
<body>
<c:import url="../includes/navbar.jsp" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="text-center">modifier un membre</h1>
        <form role="form" class="container" method="post" action="<c:url value="/admin/editmember/${user.id}"/> ">
            <div class="form-group">
                <label for="username">username:</label>
                <input type="text" class="form-control" id="username" name="username" value="${user.username}">
            </div>
            <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" id="email" name="email" value="${user.email}">
            </div>
            <p><a href="<c:url value="/admin/disableuser/" />">${user.enabled}</a></p>
            <c:choose>
                <c:when test="${isAdmin}">
                    <label class="radio-inline"><input type="radio" name="role" value="user">user</label>
                    <label class="radio-inline"><input type="radio" name="role" value="admin"
                                                       checked="checked">admin</label>
                </c:when>
                <c:otherwise>
                    <label class="radio-inline"><input type="radio" name="role" value="user"
                                                       checked="checked">user</label>
                    <label class="radio-inline"><input type="radio" name="role" value="admin">admin</label>
                </c:otherwise>
            </c:choose>
            <div class="form-group">
                <label for="firstname">prénom :</label>
                <input type="text" class="form-control" id="firstname" name="firstname" value="${user.firstName}">
            </div>
            <div class="form-group">
                <label for="lastname">nom de famille :</label>
                <input type="text" class="form-control" id="lastname" name="lastname" value="${user.lastName}">
            </div>
            <div class="form-group">
                <label for="city">ville :</label>
                <input type="text" class="form-control" id="city" name="city" value="${user.city}">
            </div>
            <div class="form-group">
                <label for="country">pays :</label>
                <input type="text" class="form-control" id="country" name="country" value="${user.country}">
            </div>
            <p><button type="submit" class="btn btn-default">Submit</button></p>
        </form>
    </div>


</div>

</body>
</html>