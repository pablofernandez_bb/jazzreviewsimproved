
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>commentaires de ${user.username}</title>
    <c:import url="../includes/bootstrap.jsp" />
</head>
<body>
<c:import url="../includes/navbar.jsp" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1>commentaires de ${user.username}</h1>
        <p><strong>total : </strong>${nbComments}</p>

        <c:forEach items="${user.comments}" var="comment" >
            <div class="well">
                <h3><a href="<c:url value="/reviewpage/${comment.review.id}" />">
                        ${comment.review.title}
                </a></h3>

                <p>${comment.content}</p>
                <p>date : ${comment.creationDate}</p>

            </div>
        </c:forEach>
    </div>
</div>

</body>
</html>
