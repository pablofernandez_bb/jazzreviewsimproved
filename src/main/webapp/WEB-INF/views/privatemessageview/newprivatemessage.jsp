<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>new message</title>
    <c:import url="../includes/bootstrap.jsp" />
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
<c:import url="../includes/navbar.jsp" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1>nouveau message</h1>

        <form role="form" method="post" action="<c:url value="/user/privatemessage/new" />" >

            <div class="form-group">
                <label for="destinationUser">destinataire</label>
                <input class="form-control" id="destinationUser" type="text" name="destinationUser" required value="${destinationUser}" />
            </div>
            <div class="form-group">
                <label for="messageBody">Votre message:</label>
                <textarea class="form-control" rows="8" id="messageBody" name="messageBody"></textarea>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
        <br/>
        <br/>
        <c:if test="${msg == 'success'}" >
            <div class="alert alert-success">message envoyé</div>
        </c:if>
        <c:if test="${msg == 'failed'}" >
            <div class="alert alert-danger">problème à l'envoi du message, veuillez vérifier le nom d'utilisateur</div>
        </c:if>
    </div>

</div>

<script>

    $('#destinationUser').autocomplete({
        source : function(requete, reponse){
            $.ajax({
                url : '<c:url value="/api/getusernames" />',
                dataType : 'json',
                data : {
                    name_startsWith : $('#destinationUser').val(),
                    maxRows : 15
                },

                success : function(data){
                    reponse(data, function(objet) {
                        return objet;
                    });
                }
            });
        }
    });



</script>

</body>
</html>