package com.jazzreviews.repository;

import com.jazzreviews.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Long> {
    
    User findByUsername(String username);

    User findById(Long id);

    List<User> findByUsernameLike(String name_startsWith);
}
