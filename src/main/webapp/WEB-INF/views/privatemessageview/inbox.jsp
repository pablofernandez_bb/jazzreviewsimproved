<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>inbox</title>
    <c:import url="../includes/bootstrap.jsp"/>
</head>
<body>
<c:import url="../includes/navbar.jsp"/>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">

        <div class="row">
        <div class="col-md-4"><h1>boite de réception</h1></div>
        <div class="col-md-3" style=" margin-top : 25px;">
            <a href="<c:url value="/user/privatemessage/new"/>">
                <button class="btn btn-primary">envoyer nouveau message</button>
            </a>
        </div>
        </div>
        <br/>
        <c:forEach var="message" items="${pms}">
            <div class="list-group">
                <a href="<c:url value="/user/privatemessage/wholemessage/${message.id}"/>"
                   class="list-group-item clearfix"><strong>${message.author}</strong> <em>
                    (envoyé à <fmt:formatDate value="${message.sendingTime}" pattern="HH:mm"/>
                    le <fmt:formatDate value="${message.sendingTime}" pattern="dd-MM-yyyy"/>)</em>
                    : ${message.excerpt}
                </a>
            </div>
        </c:forEach>

    </div>


</div>

</body>
</html>
</html>
